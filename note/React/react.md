## 小问题
### 为什么 onClick 中的函数需要绑定上 this？

js 的 this 指向问题。将 this.handleClick 赋值给 onClick 以后，组件触发事件是直接调用 onClick 的，此时 this 指向 window。

## Context
### Why

避免通过中间元素传递 props。

### What

共享对于一个组件树来说是“全局”的数据。

### When

主要应用在很多不同层级的组件需要访问同样的数据时，例如 history、locale、theme。

如果只是想避免层层传递一些属性，组件组合（component composition）有时候是一个比 context 更好的解决方案。

### How
#### React.createContext

创建一个 Context 对象。当 React 渲染一个订阅了这个 Context 对象的组件，这个组件会从组件树中离自身最近的那个匹配的 Provider 中读取到当前的 context 值。

#### Context.Provider

Provider 组件，允许消费组件订阅 context 变化。

当 Provider 的 value 值发生变化时，它内部的所有消费组件都会重新渲染。Provider 及其内部 consumer 组件都不受制于 shouldComponentUpdate 函数，因此当 consumer 组件在其祖先组件退出更新的情况下也能更新。

#### Class.contextType

将 Context 对象赋值给 class 上的 contextType 属性以后，可以通过 this.context 来消费最近 Context 上的值。

### 🌰


