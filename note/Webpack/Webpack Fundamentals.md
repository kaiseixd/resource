## 1-2 Problems with Scripts Loading

- Webpack 解决了什么问题
  - 代码合并，减少 http 请求

## 1-3 History of Modules（*）

- CommonJS 存在的问题
  - 加载慢
  - 不支持浏览器
- AMD 存在的问题
  - too dynamic of lazy loading

## 1-4 ESM (*)

## 1-5 webpack intro

- do what
  - let you write any module format
  - supports static async bundling
  - code splitting
- from
  - 2012 a code splitting issue (Tobias)
- exploded
  - 2015 bundle react app with webpack
  - hot module replacement -> motivate redux

## 1-6 config

- use CommonJS
- create with CLI
- webpack 4 mode
- use Node API

## 2-1 first time

- package.json scripts
  - 拥有 node_modules 中可执行文件的作用域
  - 可执行任意 shell 命令
  - 原理就是执行的时候会自动新建一个 shell，并执行制定命令。并会将当前目录的 node_modules/.bin 子目录加入 PATH 变量，执行结束后，再将 PATH 变量恢复原样。

## 2-5 watch

- webpack --watch
  - 在改变文件时 webpack 可以自动 compile

## 2-9 tree shaking (*)

## 2-10 （*）

- webpack bundle
- the source code of require

## 3-1 entry

- tells webpack what files to load for the browser.
- webpack trace through each of these imports, and then recursively look for other dependencies in those files until create a graph.

## 3-2 output




