### 什么是 docker？
操作系统层面的虚拟化技术，软件容器平台

### 镜像
相当于是一个 root 文件系统。除了提供容器运行时所需的程序、库、资源、配置等文件外，还包含了一些为运行时准备的一些配置参数（如匿名卷、环境变量、用户等）。

[分层存储](https://yeasy.gitbooks.io/docker_practice/basic_concept/image.html)

### 容器
镜像运行时的实体（从面向对象角度来说就是镜像的实例）。虽然容器进程运行于宿主的内核，但是它有属于自己的独立命名空间，等于是运行在一个沙箱中，与宿主环境隔离。

[存储层、数据卷](https://yeasy.gitbooks.io/docker_practice/basic_concept/container.html)

### 仓库
集中的存储、分发镜像的服务。

### docker 和传统虚拟化的区别
传统虚拟机技术是虚拟出一套硬件后，在其上运行一个完整操作系统，在该系统上再运行所需应用进程；而容器内的应用进程直接运行于宿主的内核，容器内没有自己的内核，而且也没有进行硬件虚拟。

### 为什么要使用 docker
* 更高效的利用系统资源
* 一致的运行环境：Docker 的镜像提供了除内核外完整的运行时环境，确保了应用运行环境一致性？
* 持续交付和部署
* 分层存储与高质量官方镜像

### 使用镜像
* 获取镜像：`docker pull [选项] [Docker Registry 地址[:端口号]/]仓库名[:标签]`
* 列出镜像：`docker image ls`
* 删除镜像：`docker image rm [选项] <镜像1> [<镜像2> ...]`

#### Dockerfile


### 操作容器
* 新建并启动：`docker run`，[标准操作](https://yeasy.gitbooks.io/docker_practice/container/run.html)
* 启动已终止容器：`docker container start`

### have a try
* docker image build
* docker container create
* docker container start

* docker build
* docker push

### 持续集成 CI
只要代码有变更，就自动运行构建和测试，反馈运行结果。确保符合预期以后，再将新代码"集成"到主干。

### 参考
- [ ] [写给前端的Docker实战教程](https://zhuanlan.zhihu.com/p/83309276)
- [ ] [Docker — 从入门到实践](https://yeasy.gitbooks.io/docker_practice/)

