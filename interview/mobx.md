### 简单的例子
此处应有图

### 核心概念
- state：可观察的数据结构
- derivations：在数据发生变化时自动更新的值，使用纯函数从当前 state 计算而来
- reactions：在数据发生变化时发生的副作用（比如 ReactDOM.render），最终都需要实现 I/O 操作
- actions：任一改变 state 的代码，但是在严格模式下 MobX 会强制要求使用 @action

### MobX is not a state container
与 Redux 不同，MobX 是一个用来实现函数响应式编程的库，以 observable 的方式解决状态管理问题。

### 计算值原则
任何不在使用状态的计算值将不会更新，直到需要它进行副作用（I/O）操作时。 如果视图不再使用，那么它会自动被垃圾回收。

### observable 可以处理原始值类型吗？box又有什么用？

### 为什么只有普通对象可以转变成 observable？

### 递归 observable
observable 是自动递归到整个对象的。在实例化过程中和将来分配给 observable 属性的任何新值的时候。

### autorun 依赖的

### 复习一下 decorator

### 参考
- [Mobx 中文文档](https://cn.mobx.js.org/)
- [除 Redux 外，目前还有哪些状态管理解决方案？](https://www.zhihu.com/question/63726609)

- [Mobx 作者博客](https://medium.com/@mweststrate)
