### 介绍
- [介绍](https://segmentfault.com/a/1190000012834204#articleHeader1)
- [实现目标](https://segmentfault.com/a/1190000012834204#articleHeader5) —— Fiber的主要目标是使React能够利用scheduling

### fiber
代表一个工作单元。

- [fiber的结构](https://segmentfault.com/a/1190000020110045#articleHeader2)
  - virtaul stack frame，执行完该 fiber 后返回(return) Parent fiber
  - 链表结构，包含 parent(return)/child/sibling 指针
- flush：将输出呈现在屏幕上

### suspense
让任何一个组件能够暂停渲染，等待数据的获取（比如懒加载组件、比如网络请求数据）。

- code splitting(16.6, 已上线): 文件懒加载;
- Concurrent mode(2019 年 Q1 季度): 并发模式;
- data fetching(2019 年中): 可以控制等所有数据都加载完再呈现出数据; Suspense 提供一个时间参数, 若小于这个值则不进行 loading 加载, 若超过这个值则进行 loading 加载;

### Concurrent mode
将同步的渲染变成可拆解为多步的异步渲染。



### scheduling
对 Concurrent 进行调度，支持不同渲染优先级，deadline 越近优先级越高。

- [设计原则](https://segmentfault.com/a/1190000012834204#articleHeader4) —— 关键是：
  - 在用户界面中不会立即应用每个更新，以免导致帧丢失。
- [scheduler使用优先级字段来搜索要执行的下一个工作单元](https://segmentfault.com/a/1190000012834204#articleHeader6)

### 链表结构
为了空间换时间，对于插入删除操作性能非常好。

栈结构不能随意地 continue 和 break 但是链表可以。

dfs

### fiber 的运作
- beginWork：beginWork 會執行 component 實體化（instantiate）、呼叫 component 的 render() 方法、以及進行 shouldComponentUpdate() 結果的比較。
- completeWork：completeWork 會進行 effectTag 的設定來標記副作用（Side Effect），以及 Host 的實體化等工作。（completeWork 只會在末端的 Host 執行）
- commitWork：commitWork 則會呼叫 componentDid(Mount|Update) 等 Lifecycle method，以及將基於 completeWork 設定的 effectTag 的結果反映給 Host。
(commitWork 則會呼叫 componentDid(Mount|Update) 等 Lifecycle method，以及將基於 completeWork 設定的 effectTag 的結果反映給 Host。)

### Render 阶段
协调算法始终使用 renderRoot  函数从最顶层的 HostRoot 节点开始。不过，React 会略过已经处理过的 Fiber 节点，直到找到未完成工作的节点。例如，如果在组件树中的深层组件中调用 setState 方法，则 React 将从顶部开始，但会快速跳过各个父项，直到它到达调用了 setState 方法的组件。

### 权重
權重較低的任務會用 requestIdleCallback 在閒置時執行，權重較高的任務則會使用 requestAnimationFrame 盡快的同步 render 出來。
權重低的任務在執行時，若有權重高的任務進來，則權重低的任務會被中斷，並執行權重高的任務。權重高的任務結束後，再繼續執行權重低的任務。（beginWork会被打断，commitWork不会?）
(https://medium.com/@_cybai/%E7%BF%BB%E8%AD%AF-react-fiber-%E7%8F%BE%E7%8B%80%E7%A2%BA%E8%AA%8D-fd3808072279)

### 如何决定每次更新的数量

### Error Boundary
Error Boundary 是可以在當 child component 的 render 出錯時用來將 parent 重新 render 的機制。
(https://medium.com/@_cybai/%E7%BF%BB%E8%AD%AF-react-fiber-%E7%8F%BE%E7%8B%80%E7%A2%BA%E8%AA%8D-fd3808072279)

### fiber的应用
- Suspense
- Error Boundaries
- new lifecycle（在 fiber 架构下, render 前的钩子会被多次调用, 在 componentWillMount 里执行订阅事件就会产生内存泄漏）

### todo
- scheduler如何找到要执行的下一个工作单元。
- 如何通过fiber树跟踪和传播优先级。
- scheduler如何知道何时暂停和恢复工作。
- 工作如何被刷新并标记为完整。
- 副作用（如生命周期方法）如何工作。
- 协程是什么以及如何用它来实现上下文和布局等功能。

### 如何阅读源码
yarn build core,dom --type=UMD
fixtures/

### 参考
- [ ] [A Cartoon Intro to Fiber](https://www.youtube.com/watch?v=ZCuYPiUIONs&list=PLb0IAmt7-GS3fZ46IGFirdqKTIxlws7e0&index=5)
- [ ] [react fiber 主流程及功能模块梳理](https://juejin.im/post/5c70f044f265da2de4507ab9)
- [ ] [React Fiber架构](https://zhuanlan.zhihu.com/p/37095662)
- [x] [React Fiber Architecture](https://segmentfault.com/a/1190000012834204)
- [ ] [精读《Scheduling in React》](https://juejin.im/post/5cb3d81e6fb9a068af37a564)
- [ ] [浅谈React Scheduler任务管理](https://zhuanlan.zhihu.com/p/48254036)
- [x] [调度的设计原则](https://zh-hans.reactjs.org/docs/design-principles.html#scheduling)
- [ ] [完全理解React Fiber](http://www.ayqy.net/blog/dive-into-react-fiber/)
- [ ] [如何理解 React Fiber 架构？](https://www.zhihu.com/question/49496872/answer/116346458)
- [ ] [from-jsx-to-dom](https://github.com/xieyu/blog/blob/master/src/react/from-jsx-to-dom.md)
- [ ] https://github.com/funfish/blog/issues/28
- [ ] http://www.sosout.com/2018/08/12/react-source-analysis.html
- [ ] https://github.com/RubyLouvre/anu/tree/master/packages/fiber
- [x] https://medium.com/@_cybai/翻譯-react-fiber-現狀確認-fd3808072279

Max Koretskyi 的三篇文章：
- [x] [Inside Fiber: in-depth overview of the new reconciliation algorithm in React](https://medium.com/react-in-depth/inside-fiber-in-depth-overview-of-the-new-reconciliation-algorithm-in-react-e1c04700ef6e)
- [x] [The how and why on React’s usage of linked list in Fiber to walk the component’s tree](https://medium.com/react-in-depth/the-how-and-why-on-reacts-usage-of-linked-list-in-fiber-67f1014d0eb7)
- [ ] [In-depth explanation of state and props update in React](https://medium.com/react-in-depth/in-depth-explanation-of-state-and-props-update-in-react-51ab94563311)

- [ ] [这可能是最通俗的 React Fiber(时间分片) 打开方式](https://juejin.im/post/5dadc6045188255a270a0f85#heading-0)