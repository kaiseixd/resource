**react router**
### 为什么react-router要用自己的location和history？

### 参考
- [由浅入深地教你开发自己的 React Router v4](https://segmentfault.com/a/1190000009004928)


**reconciliation**
### 为什么需要
react 在更新时会对整个应用进行重新渲染，需要 reconciliation 优化更新开销。

### 介绍


**virtual dom**
### 介绍
- [高级描述如下所示](https://segmentfault.com/a/1190000012834204#articleHeader4) —— 高级描述如下所示


**fiber**


**git**
### 回退

### cankao
https://github.com/geeeeeeeeek/git-recipes/wiki/5.1-%E4%BB%A3%E7%A0%81%E5%90%88%E5%B9%B6%EF%BC%9AMerge%E3%80%81Rebase-%E7%9A%84%E9%80%89%E6%8B%A9

**数据结构**
https://github.com/trekhleb/javascript-algorithms

**css**
https://juejin.im/post/5d40120f6fb9a06b0471d956

**浏览器**
https://juejin.im/post/5d89798d6fb9a06b102769b1

**性能优化**
[React 16 加载性能优化指南](https://juejin.im/post/5b506ae0e51d45191a0d4ec9)

**koa**
### 介绍
- [egg&koa](https://eggjs.org/zh-cn/intro/egg-and-koa.html)

### co
- [koav1 & co](https://github.com/SamHwang1990/blog/issues/3)

### async
- [解读Promise & Async Functions & Koa@v2](https://github.com/SamHwang1990/blog/issues/10)

**模块化**
[循环依赖](https://zhuanlan.zhihu.com/p/33049803)

**promise**

**前端工程化**

**跨域**

**react**
### react 和 vue 的比较

### virtual dom

### diff

**vue**
### 双向绑定


**负责的项目**
简单自我介绍, 做过哪些项目, 使用哪些技术栈

技术选型、前端工程化、架构、设计模式、复杂模块、性能、安全问题、团队管理

从我个人的业务出发，基本上把项目开发中所有我自己认为可以展示自己思考的点都问了一遍

没事给新来的同学梳理业务，画画业务框架图，主动承担一些有技术难度的工作（比如性能优化、安全排查、工具上提升开发效率等），多分享自己的工作，多和产品经理撕逼，锻炼自己的表达总结能力

**业务思考**
- [业务面试](https://zhuanlan.zhihu.com/p/84087044)

**你有什么想问我的？**
目前咱们的业务，有一个什么样时间规划，这期间有哪些milestone?
我的过往工作经历，有哪些是对咱们团队有帮助的？
您对我在团队中的定位是怎么样的？
对于团队成员的成长，您有哪些方案？

**面试题**
https://zhuanlan.zhihu.com/p/84212558
https://zhuanlan.zhihu.com/p/83801858

**代码题**
去抖
[event emitter](https://github.com/sunyongjian/blog/issues/13)
compose
jsonp

### 代码分割
- 使用 vendor 将业务代码和第三方库分离
  - SplitChunksPlugin
    - chunks：async（只优化 import）、initial（只优化正常引入公共部分）、all
    - cacheGroups
- import()

#### 参考
- [Webpack 大法之 Code Splitting —— 已经过时](https://zhuanlan.zhihu.com/p/26710831)
- [Webpack SplitChunksPlugin chunks 的三种模式](https://github.com/wayou/wayou.github.io/issues/40)
- [一步一步的了解webpack4的splitChunk插件](https://juejin.im/post/5af1677c6fb9a07ab508dabb#heading-7)

### 最佳实践


#### 参考
[Webpack 4 配置最佳实践](https://juejin.im/post/5b304f1f51882574c72f19b0#heading-3)


### runtimeChunk
持久化缓存使用
https://segmentfault.com/q/1010000014954264
内联 manifast chunk 到 index.html

### sourceMap
devtool: true
SourceMapDevToolPlugin
source-map-loader

### css cache

### ssr
https://www.yuque.com/es2049/blog/zy0eq0

### github clone speedup
git config --global http.https://github.com.proxy socks5://127.0.0.1:1086
git config --global https.https://github.com.proxy socks5://127.0.0.1:1086

git config --global http.proxy socks5://127.0.0.1:1086
git config --global https.proxy socks5://127.0.0.1:1086