### useCallback
缓存了 inline callback 的实例，可以不在每次 re-render 时重新创建。

`useCallback(fn, inputs) === useMemo(() => fn, inputs)) `

通过 useRef().current 可以拿到 input 组件的当前 value，但是又不会因为输入时 value 改变而重新创建函数：
```js
const textRef = useRef();

useLayoutEffect(() => { // 相当于 mount 和 update
  textRef.current = text; // 将 text 写入到 ref
});

const handleSubmit = useCallback(() => {
  const currentText = textRef.current; // 从 ref 中读取 text
  alert(currentText);
}, [textRef]); // 这样 callback 就只用缓存一次
```

[useCallback() invalidates too often in practice](https://github.com/facebook/react/issues/14099#thread-subscription-status)

### useEffect
最难用的

### useRef
存放不会变化的组件实例属性

### useLayoutEffect

### 组件间通信
[频繁组件间通信使用 React.useContext](https://segmentfault.com/a/1190000020329053#item-2-6)

### reference
* [x] [精读《React Hooks 最佳实践》](https://segmentfault.com/a/1190000020329053)
* [x] [React Hooks 第一期：聊聊 useCallback](https://zhuanlan.zhihu.com/p/56975681)
* [ ] [精读《useEffect 完全指南》](https://github.com/dt-fe/weekly/blob/v2/096.%E7%B2%BE%E8%AF%BB%E3%80%8AuseEffect%20%E5%AE%8C%E5%85%A8%E6%8C%87%E5%8D%97%E3%80%8B.md)
* [ ] [fefame](https://zhuanlan.zhihu.com/fefame)
* [ ] [faq](http://react.html.cn/docs/hooks-faq.html)