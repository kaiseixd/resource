### introducing-reactivity

```js
let update
const onStateChanged = _update => {
    update = _update
}

const setState = newState => {
    state = newState
    update()
}

onStateChanged(() => {
    view = render(state)
})
```

- using `Object.defineProperty` to convert state objects to become reactive

### getters & setters

```js
function convert(object) {
    Object.keys(object).forEach(key => {
        let internalValue = object[key];
        Object.defineProperty(obj, key, {
            get() {
                console.log(`get ${key}: ${internalValue}`)
                return internalValue
            }
            set(newValue) {
                console.log(`set ${key}: ${newValue}`)
                internalValue = newValue
            }
        })
    })
}
```

### dependency tracking


