### compare with redux
#### what's mobx
* different view on state management
* less verbose
* based on a fully mutable state. Every mutations will subscribe changes.
* 与 context provider 相比
  * always keep a reference to the same state object. No need to update the whole contained tree on state change.

#### what's redux
* have to deal with actions and reducers to produce a new state based on dispatched actions and the previous state.
* immutable
* module?

### 一些概念？
* provider
* connect
* selector
* module

### 性能比较
#### 为什么一些场景下 mobx 性能更好
* [why mobx faster](https://hackernoon.com/becoming-fully-reactive-an-in-depth-explanation-of-mobservable-55995262a254)
  * redux 会过度订阅，造成没有意义的重复渲染。而 mobx 的设计则是：一个运行时决定的最小订阅子集
  * 办法非常的简单，所有的数据都不会被缓存，而是统统通过派生（derive）计算出来（如果你了解 Mobx 你应该知道 derivation 的概念，它代指 computed value 和 reactions）
  * Mobx 并不会计算所有的派生值，而是计算那些目前处于 observable 状态中的

#### redux
[Redux的性能问题](http://cn.redux.js.org/docs/faq/Performance.html)

#### global Provider
* redux
  * 由于 immutable data 的关系，一旦列表中某项数据内容发生改变，会导致整个列表都被重新渲染

### 问题
#### 不能深层次 observe object
但是可以用 boxed values

#### re-rendering
* observer HOC：使用了 React.memo，可以减少 re-render
* observer component：只会 re-render 当前组件
  * 有没有 useMemo 呢？
* useObserver：比较底层的方法，只要 observe 的数据改变就会 re-render，需要自己定制优化
* 使用 useMemo 解决 useContext 造成不必要重新渲染的问题

#### useInject?
* [why no useInject](https://github.com/mobxjs/mobx-react-lite/issues/6)
* [HOC inject example](https://github.com/mobxjs/mobx-react-lite/issues/36)
* [Store injecting](https://mobx-react.js.org/recipes-inject)

### example
#### track form value（Field）
* [Create your own Formik with React Hooks and MobX](https://levelup.gitconnected.com/formik-with-react-hooks-and-mobx-1493b5fd607e)
* [code](https://codesandbox.io/embed/5y2pprm17p?source=post_page-----1493b5fd607e----------------------)

* 不需要重复初始化的对象使用 useRef 存储（mobx store）

### reference
* [Create your own Formik with React Hooks and MobX](https://levelup.gitconnected.com/formik-with-react-hooks-and-mobx-1493b5fd607e)