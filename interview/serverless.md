### Serverless 对前端的影响
前端工程师能够基于 Serverless 去开发函数、实现后端功能。而后端工程师则去实现不适用函数编写的功能，或者供函数使用的一些微服务。

前端需要处理的后端变得更简单（运维），后端变得更靠后。

### 参考
- [ ] [Serverless 应用开发指南](https://serverless.ink/)
- [x] [阿里蒋航：Serverless 将使前后端从分离再度走向融合](https://www.infoq.cn/article/bodZTULS2LQ-G4UIahjf)