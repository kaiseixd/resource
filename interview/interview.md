### refer

https://github.com/Nealyang/PersonalBlog/issues/48
https://juejin.im/post/59ddb609f265da431f4a0b30
https://segmentfault.com/a/1190000013508719
https://segmentfault.com/a/1190000015666655#articleHeader0
https://github.com/dwqs/blog/issues/68
https://github.com/jawil/blog/issues/2
https://segmentfault.com/a/1190000015648248

### 学习方法

- 学习一门语言的新语法，其实不应该局限于其用法，而应当尝试去了解其背后的理念，**其想解决的问题**。

### 开始

- [ ] 遇到最难的问题是什么
- [ ] 技术的使用场景
- [ ] 你觉得你最擅长什么
- [ ] 哪些出彩的地方，有什么成果
- [ ] mock如何做的
- [ ] proxy如何做的
- [ ] eslint如何做的

### 重要
#### js
- [ ] 继承


#### css
- [ ] [说一说BFC/IFC/FC](https://www.yuque.com/kaisei/fe/kmg0a2)
  - [触发方式](https://www.yuque.com/kaisei/fe/kmg0a2#rpZLp)
- [ ] 介绍一下盒模型

#### React
- [ ] [fiber](https://www.yuque.com/kaisei/fe/ei7p81)
  - 将 js 中的同步更新过程进行分片，每执行完一段更新过程，就把控制权交还给React负责任务协调的模块，看看有没有其他紧急任务要做，如果没有就继续去更新。
- [ ] [key的作用是什么？](https://www.yuque.com/kaisei/fe/kl82ty)
- [ ] [HOC](https://www.yuque.com/kaisei/fe/sdey7n)
- [ ] diff 完之后的更新
- [ ] react 16 diff
- [ ] fiber
  - https://zxc0328.github.io/2017/09/28/react-16-source/
  - https://zhuanlan.zhihu.com/p/58863799
- [ ] [diff演变](https://segmentfault.com/a/1190000011235844)

#### 浏览器
- [ ] [重绘/回流](https://www.yuque.com/kaisei/fe/lqg5gy)
- [ ] [缓存](https://www.yuque.com/kaisei/note/hbnmnw)
- [ ] 浏览器加载
- [ ] 浏览器渲染

#### git
- [ ] Git
  - merge
  - rebase
  - cherry-pick
  - reset
  - [工作流](https://juejin.im/post/5a014d5f518825295f5d56c7#heading-25)
  - 分支管理模式
  - 如何使用分支避免代码冲突

#### 模块化
- [ ] 讲一下模块化
- [ ] [webpack](https://www.yuque.com/kaisei/fe/aq3m5i)

#### 前端基础
- [ ] [性能优化](https://www.yuque.com/kaisei/fe/gzrb46)
- [ ] 跨域
- [ ] 闭包
- [ ] 防抖节流
- [ ] [event loop](https://www.yuque.com/kaisei/fe/iwuttx)
  - task和渲染的时机
  - [node event loop](https://www.yuque.com/kaisei/fe/iwuttx#wOCNH)


#### 移动端
- [x] [移动端高清（1px）解决方案](https://juejin.im/entry/5aa09c3351882555602077ca)：postcss-px-to-viewport

#### 网络
- [ ] [分层模型](https://www.yuque.com/kaisei/fe/cxpinz)
- [ ] [cdn的原理和作用](https://github.com/sunyongjian/blog/issues/41)
- [ ] cookie与session
- [ ] HTTPS建立连接的过程
- [ ] HTTP/HTTPS/HTTP 2.0优缺点


#### 安全
- [ ] 安全
  - CSP
  - 如何发起CSRF攻击？


#### 计算机基础
- [x] 讲一下了解的设计模式（观察者模式和发布订阅模式的区别）
- [ ] 算法
  - [遍历](https://www.yuque.com/kaisei/fe/fvgt6t)
  - [排序](https://www.yuque.com/kaisei/fe/zzm2iy)

### 简单

- [ ] typeof & instaneof
- [ ] 类型转换(包括 ==)
- [ ] Symbol.hasInstance
- [ ] this 的几种情况
- [ ] 介绍下 Set、Map、WeakSet 和 WeakMap 的区别？
- [ ] life of a frame
- [ ] margin的一些问题（合并、行内垂直无效、负值）
- [ ] MVC/MVVM

### 麻烦

- [ ] immutable
- [ ] 如何对项目进行监控，错误处理
- [ ] 证书验证
- [ ] withRouter

### 比较偏

- [ ] 尾递归
- [ ] 了解什么linux的常用命令
- [ ] websocket
- [ ] eslint工作原理
- [ ] mobx，以及如何实现一个
- [ ] [硬件加速](https://www.yuque.com/kaisei/fe/aa9098)

### 笔试

- [ ] Promise
- [ ] EventEmitter
- [ ] list to tree
- [ ] currying
