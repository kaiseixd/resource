## redux
### 基本思想

- 维护一个顶层数据仓库，称为store。
- 全局只能存在一个store，所有的状态都存在于其中。
- 有一些被称为reducer的方法，例如reducer/theme.js，它是唯一可以准许被修改store中state的方法，每个reducer对应一个state，他接受上一次的state、一个action的type和可选的修改量，根据type来进行操作并返回下一级的state。
- 一些被称为action的东西，例如action/index.js，至于说东西，因为这个东西比较抽象，只能说它定义了一些可预测的行为，他可以是type，也可以是一些带有副作用的方法，一般和dispatch方法配合使用。
- dispatch方法是Redux的核心方法，它是触发action的唯一途径。

### store

### reducer

```js
// reducer 必须返回 state
currentState = currentReducer(currentState, action)
```

### action

### applyMiddleware

### 参考

https://github.com/jnotnull/dva-generator/issues/5


## 安全
### rel="noopener noreferrer"

对于使用 target='_blank' 打开的新页面，window 对象上会有一个属性 opener，指向前一个页面的 window 对象。这样就能通过 window.opener.location 更改原页面地址。

### XSS

### CSRF

### getBoundingClientRect

### Generator

### async await


## render
### 子组件的渲染时机？

- 父组件渲染？
- props 改变？
- 深层次的 props 改变？
- ...





## 待解决列表

xss、csrf

http、https

面向对象、函数式编程的理解

for-in，Object.keys 的枚举问题

私有变量

use strict
    没有变量提升

16 context
https://zhuanlan.zhihu.com/p/42654080

## 算法
斐波那契数列？怎么优化？
从数组中找出三数之和为n
二分查找的时间复杂度怎么求？

## 浏览器渲染过程


## 作用域


## 从 Module 出发
### ES6 Module

ES6 模块在编译时就完成了模块加载（静态加载），import 的并不是对象（CommonJS 是），而只会生成一个只读的引用，等到脚本真正执行时，再根据这个只读引用，到被加载的那个模块里面去取值。

CommonJS 则是直接拿到拷贝的对象。

### js 到底是解释型语言还是编译型语言？

是否有预编译：js 本身是没有预编译，但是在浏览器运行 js 之前必须将其编译成客户端的机器码。

解释型语言虽然也要将编程语言转换成机器可以理解的语言，但是是在运行时转换的，所以需要将解释器安装在环境中，而编译型语言编写的应用在编译后能直接运行。

如果是解释型语言那么为什么会有变量提升？为什么会有 JIT（还未做出回答）？

结论：

js 需要环境中有引擎才能执行，说明是解释型语言。变量提升只是 JS 引擎在解析并创建作用域时的产物，并没有生成中间代码。

再问：JIT 是什么时候进行优化的？和引擎的 parse analyze 是同一时刻？

- [JavaScript有预编译吗？](https://www.zhihu.com/question/29105940)
- [JavaScript到底是解释型语言还是编译型语言?](https://segmentfault.com/a/1190000013126460)

### 变量提升是怎么实现的？

V8 引擎 parse 一遍 js 代码得出 AST，然后 analyze 一遍，在每层作用域都会维护一个独立的声明作用域（`variables_` 表），这样运行时遇到赋值或者取值就可以从声明作用域中递归（内往外）查找变量。为了处理一些不能确定的特殊情况，V8 会将 proxy 与变量的绑定推迟到 Analyze 阶段。

analyze 完生成 CPU 可以执行的机器码。

所以变量提升是在创建执行上下文时实现的，解释器收集了声明的变量，执行时再从作用域中取值。

> 虽然 v8 有“预语法分析”,「[公式]」， 只是为了收集信息辅助后续加速的，不属于预编译。
> 引擎每一次遇到声明，它就把声明传到作用域来创建一个绑定。对每一次声明它都会为变量分配内存。只是分配内存而不是把代码修改成声明提升。正如你所知道的，在JS中分配内存意味着将默认值设为undefined。

### V8引擎、SpiderMonkey、JIT

解释型语言的一个问题是每条语句都是分开转换的，无法做优化，如果一个 1000 次的 for 循环那就要转换 1000 次。

> 在JavaScript中如果一段代码运行超过一次，那么就称为warm。如果一个函数开始变得更加warm（译者注：运行更多次），JIT将把这段代码送到编译器中编译并且保存一个编译后的版本。下一次同样代码执行的时候，引擎会跳过翻译过程直接使用编译后的版本。

- [WebAssembly 系列（二）JavaScript Just-in-time (JIT) 工作原理](https://zhuanlan.zhihu.com/p/25669120)
- [A crash course in just-in-time (JIT) compilers](https://hacks.mozilla.org/2017/02/a-crash-course-in-just-in-time-jit-compilers/)

* todo嗷

### WebAssembly

- [WebAssembly 系列（一）生动形象地介绍 WebAssembly](https://zhuanlan.zhihu.com/p/25800318)
- [来谈谈WebAssembly是个啥？为何说它会影响每一个Web开发者？](https://blog.csdn.net/wulixiaoxiao1/article/details/60581397)

* todo嗷

### js的内存与堆与栈？

* todo嗷

## React 怎么判断什么时候该重新渲染组件？

state 变化？props 变化？setState 之后是怎么触发重新 render 的？props 改变之后又是怎么触发的？

## React 性能优化

https://zh-hans.reactjs.org/docs/optimizing-performance.html#shouldcomponentupdate-in-action

## TypeScript or babel 是如何编译 class 的？

## ES6 和 CommonJS 相比

[ECMAScript 6 的模块相比 CommonJS 的require (...)有什么优点？](https://www.zhihu.com/question/27917401/answer/223309781)

## babel polyfill

[Babel 编译出来还是 ES 6？难道只能上 polyfill？](https://www.zhihu.com/question/49382420)

## 知乎聚聚关注的问题？

yhj、tz、hsj

## diff 流程

- [解析vue2.0的diff算法](https://github.com/aooy/blog/issues/2)

## 编程题

- 遍历
- flatternDeep
- 防抖节流
- 实现一个new
- 实现一个 sleep 函数，比如 sleep(1000) 意味着等待1000毫秒，可从 Promise、Generator、Async/Await 等角度实现
- 使用迭代的方式实现 flatten 函数

## BFChttps://github.com/youngwind/blog/issues/108

## virtual dom
### 是什么

### 为什么需要

### 和原生DOM操作相比？



### 参考
- [面试官: 你对虚拟DOM原理的理解?](https://juejin.im/post/5d3f3bf36fb9a06af824b3e2)

## 继承

## Hook API

## 性能优化

## 从class迁移
### 生命周期
#### getDerivedStateFromProps

```js
function ScrollView({row}) {
  let [isScrollingDown, setIsScrollingDown] = useState(false);
  let [prevRow, setPrevRow] = useState(null);

  if (row !== prevRow) {
    // Row 自上次渲染以来发生过改变。更新 isScrollingDown。
    setIsScrollingDown(prevRow !== null && row > prevRow);
    setPrevRow(row);
  }

  return `Scrolling down: ${isScrollingDown}`;
}
```

### shouldComponentUpdate

React.memo

### 其他生命周期

- componentDidMount, componentDidUpdate, componentWillUnmount：useEffect Hook 可以表达所有这些。
- componentDidCatch and getDerivedStateFromError：目前还没有这些方法的 Hook 等价写法，但很快会加上。

### how to fetch data?



## Hook
### 自定义Hook

- [ ] 每次使用它的时候都等于是重新执行一遍吗？

### 参考

- [官方文档](https://zh-hans.reactjs.org/docs/hooks-intro.html)


## Hook 进阶

### 参考
- [Making Sense of React Hooks](https://medium.com/@dan_abramov/making-sense-of-react-hooks-fdbde8803889)
https://juejin.im/post/5d594ea5518825041301bbcb
https://github.com/dt-fe/weekly/blob/v2/079.%E7%B2%BE%E8%AF%BB%E3%80%8AReact%20Hooks%E3%80%8B.md
https://github.com/dt-fe/weekly/blob/v2/080.%E7%B2%BE%E8%AF%BB%E3%80%8A%E6%80%8E%E4%B9%88%E7%94%A8%20React%20Hooks%20%E9%80%A0%E8%BD%AE%E5%AD%90%E3%80%8B.md
https://github.com/dt-fe/weekly/blob/v2/096.%E7%B2%BE%E8%AF%BB%E3%80%8AuseEffect%20%E5%AE%8C%E5%85%A8%E6%8C%87%E5%8D%97%E3%80%8B.md

## 生命周期
### 从旧生命周期向新生命周期迁移

## Render Props（与HOC）

指一种在 React 组件之间使用一个值为函数的 prop 共享代码的简单技术

https://zh-hans.reactjs.org/docs/render-props.html#___gatsby

## ts in react
### FC
```js
const App: React.FC<{ message: string }> = ({ message }) => (
  <div>{message}</div>
);

// same as
type AppProps = { message: string }; /* could also use interface */
const App = ({ message }: AppProps) => <div>{message}</div>;
```

## render

1. 父组件render对子组件生命周期和render的影响
2. scu对render的影响

## edm compose

- Arpeggio
- saw wave lead
- pluck
- ride
- pad
- acid
- mid bass
- sub bass
- layer
- chords
- massive

- breakdown
- drop
- buildup