大喵教育前端培训
================

## 阶段性测试 2018.05.25

### 大喵教育版权所有 | 出题人：谢然


01. 用文字描述如下选择器将选择哪些（个）元素
    ```css
    div, h1 {}
    div[class] [id="abc"] {}
    div:hover ul li > div {}
    body :active {}
    div:hover::after {}
    ::selection {}
    :target {}
    input + ul + p ~ span {}
    ```

    * 所有div和h1元素
    * 包含class的div元素的id为abc的后代元素
    * 被hover的div元素的后代ul的后代li的子代div
    * body的后代被激活（按下按键）元素
    * 被hover的div的after伪元素
    * 被用户选中的高亮部分
    * 目标元素
    * input元素的向下相邻兄弟ul的向下相邻兄弟p的所有向下兄弟span

02. 分别写出如下几个选择器的优先级
    ```css
    * * * {}
    div * span {}
    div[title] {}
    fieldset legend + input {}
    #some #thing .not:hover .abc:hover {}
    ```

    * 0
    * 0, 0, 0, 2
    * 0, 0, 1, 1
    * 0, 0, 0, 3
    * 0, 2, 2, 2

03. https://www.example.com/a/b/ 页面中有如下代码
    ```html
    <link rel="stylesheet" href="//test.example.com/path/../the/../path/c.css">
    ```
    请问最终引入的c.css的完整路径是什么？

    * `https://test.example.com/path/c.css`

04. `em,px,rem,vw,vh` 分别代表多长？

    * 当前 font-size, 像素, HTML 根元素 font-size, 视口宽度,  视口高度

05. 显示器的物理分辨率为 `1920x1080`，操作系统设置的分辨率为 `1280x720`，网页的放大倍数为 `110%`，请计算一个 CSS 像素对应多少个显示器物理像素（面积与长度）？

    * 1.65

06. 写出如下代码显示在浏览器后**每个单词**的字号
    ```html
    <style>
      html {
        font-size: 20px;
      }
      section {
        font-size: 10rem;
      }
      p {
        font-size: 24px;
      }
      span {
        font-size: 150%;
      }
      .sucks {
        font-size: inherit;
      }
    </style>
    <body>
      <section>
        <h2>Brown</h2>
        <p>quick</p>
        <p>jumps <span>over <span>lazy</span> dog</span></p>
        <p class="sucks">sucks</p>
      </section>
    </body>
    ```

    * Brown: 200px;  （300px， 默认1.5em）
    * quick: 24px;
    * jumps: 24px;
    * over, dog: 36px;
    * lazy: 54px;
    * sucks: 200px;

07. 字体的 italic 与 obsolete 的区别是？

    * italic 是由字体专门提供的倾斜字体，而且可能会占更少的高度； oblique 是由 normal 字体计算而来

08. 写出满足如下条件的选择器
    ```
    1. 第 8 个子结点之后，倒数第 5 个子结点之前的li结点
    2. 【类名】以“damiao-”开头的元素
    3. rel 属性中有 nofollow 这个单词的标签
    ```

    * li:nth-child(n+9):nth-last-child(n+6)
    * [class^="damiao-"]
    * [rel~="nofollow"]

09. 链接伪类的几种状态书写的顺序是什么？为什么？

    * LVHA：:link -> :visited -> :hover -> :active
    * 否则样式会被覆盖

10. 如下 font 属性的值哪一个是书写正确的？
    ```
    1. font: serif 24px;
    2. font: serif bold 24px/1.2;
    3. font: bold 24px/1.2 serif;
    ```
    * 3

11. vertical-align 取 middle 时元素如何对齐？

    * 对齐父元素 baseline 向上 1/2 ex

12. 什么是 baseline？

    * 文字的书写基线，位置在小写 x 字体的底部（不包括空隙）

13. 详述你对盒模型的理解。

    * 元素在文档中会以矩形盒子显示，盒子中包含多个框，这些框由内向外分别是 content, padding, border, margin
    * 盒子的宽高默认为 content-box 的宽高，但是可以通过 box-sizing 属性修改为 border-box
    * 盒子的背景到 border-box 为止，但是可以通过 background-clip 属性修改为其他

14. 如何让一个元素可被 focus？如何去掉其被 focus 时的虚框？

    * 给元素设置 tabindex 属性（值为负数的话不会被 tab 到）
    * outline: none;0

15. 如何给css添加注释

    * /* outline: none; */

16. 指出如下css代码中的错误
    ```
    p,h1,{
        
        background-color: rgba:(abc)
        font-varient; abc;
        colr: #ff048;
        font: "serif" 25px;
    }
    ```

    * rgba(0, 0, 0, 0)
    * font-variant
    * color: #ffffff
    * font: 25px serif;

17. 元素的高度写百分比在什么情况下【无效】，为什么？在什么情况下【有效】，有效时是以哪个元素的高度为基准值？

    * 在非绝对定位的情况下，父元素高度为 auto 时无效
    * 使用绝对定位或者父元素有确定值
    * 正常情况下基于父元素 content-box 高度，绝对定位时基于最近的定位祖先元素的 padding-box 高度

18. 解释 box-sizing 可以取哪些值，以及每个值的意义

    * content-box：width、height和内容区宽高一致，元素 width = width + padding-left + padding-right + border-left + border-right
    * border-box：width、height即元素宽高，元素 width = width

19. 如下结构中，div 有两个伪元素，分别标出伪元素的位置，用 `<before></before>` 表示 `::before` 伪元素，用 `<after></after>` 表示 `::after` 伪元素
    ```html
    <div>
      <h1>The article</h1>
      <p>the quick brown fox</p>
    </div>
    ```

    * 解答如下：

    ```html
    <div><before></before>
      <h1>The article</h1>
      <p>the quick brown fox</p>
    <after></after></div>
    ```

20. 如何在伪元素中插入换行符？如何让这个换行符在页面中生效？

    * content: '\a';
    * white-space: pre;

21. 简述 ie7 市场份额比 ie6 低的原因并在网络上找出目前各大浏览器在中国和全球的市场份额

    * 因为 xp 系统自带的是 ie6 浏览器
    * 18年2月市场份额：
    * 中国：chrome(46%), IE(23%), QQ(7%), 2345(6%), Sougou(4%), Firefox(2%), 其他(12%)
    * 全球：chrome(67%), Firefox(12%), IE(7%), Edge(5%), safari(4%), opera(2%), 其他(2%)

22. 页面有无 `doctype` 声明会有什么区别？

    * 没有的话页面会以怪异模式渲染

23. 有一张高为 100 宽为 50 的图片，内有一个直径为 40 的圆，其做为一个 200x200 的元素的背景图片，background-size 为 contain 和 cover 时，圆的直径分别为多少？

    * 80, 160

24. 写出实现小米网首页 logo 返回主页的动画效果的代码。

    * 解答如下：

    ```css
    .mi {
      width: 50px;
      height: 50px;
      background: url('./img/mi-home.jpg') -50px 0 no-repeat;
      transition: all 0.2s;
      cursor: pointer;
    }
    .mi:hover {
      background-position-x: 0;
    }
    ```

25. 给出至少 5 种水平垂直居中方案。

    * 解答如下：
    ```html
    <style>
      .parent {
        width: 200px;
        height: 200px;
        background: aqua;
      }
      .child {
        width: 100px;
        height: 100px;
        background: deeppink;
      }
    </style>
    <div class="parent">
      <div class="child">Lorem ipsum dolor sit amet.</div>
    </div>
    ```

    ```css
    /* vertical-align: middle */
    .parent {
      line-height: 200px;
      text-align: center;
    }
    .child {
      display: inline-block;
      vertical-align: middle;
      line-height: normal;
      text-align: left;
    }
    /* vertical-align: middle || version 2 */
    .parent {
      text-align: center;
      font-size: 0;
    }
    .parent::after {
      content: '';
      display: inline-block;
      height: 50%;
    }
    .child {
      display: inline-block;
      vertical-align: middle;
      text-align: left;
      font-size: 16px;
    }
    /* display: table-cell */
    .parent {
      display: table-cell;
      vertical-align: middle;
    }
    .child {
      margin: auto;
    }
    /* position: absolute */
    .parent {
      position: relative;
    }
    .child {
      position: absolute;
      top: 0; right: 0; bottom: 0; left: 0;
      margin: auto;
    }
    /* transform: translate(-50%, -50%) */
    /* 可用负 margin 代替  */
    .parent {
      position: relative;
    }
    .child {
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
    }
    /* display: flex */
    .parent {
      display: flex;
      justify-content: center;
      align-items: center;
    }
    ```

26. 简述 em 框，内容区，行内框，行框的构成以及其需要注意的问题。

    * em框
      * 由字符构成
      * 实际字形可能比 em 更高或更矮
    * 内容区
      * 由 em 框构成
      * 是 inline 元素的背景区域，或者鼠标选中能变高亮的部分
      * 替换元素的内容区是固有高度加上 margin, border, padding
    * 行内框
      * 由内容区加上行间距构成
      * 非替换元素的行内框高度刚好是 line-height
      * 由于替换元素没有行间距，所以其行内框高度就是内容区高度
    * 行框
      * 由多个行内框构成

27. 如何确定一个行内框的baseline及其最高点和最低点？

    * baseline 由 vertical-align 值为 baseline 的内容区中小写 x 的底部确定
    * 选中其中的字，通过高亮框确定

28. `td` 元素的 `headers` 属性是干嘛的？
 
    * 规定关联的 th，主要用于屏幕阅读器

29. color 这个属性有什么需要注意的地方？

    * 透明色 transparent
    * 当前文字颜色 currentColor

30. 如何理解 inline-block 元素？它有什么需要注意的地方？

    * 外面是 inline 框，里面是 block 框
    * 使用时需要做一些处理消除标签之间的空白符影响
      * 父元素设置 font-size: 0;
      * margin 负值
      * 父元素设置 letter-spacing: -3px;
      * 父元素设置 word-spacing: -6px;

31. 什么是 CSS Sprite？为什么要使用 CSS Sprite？它有哪些优缺点？

    * 将网页中需要用到的图标都放进一张图里，通过更改 background-position 切出所需图标
    * 优点：只需发起一次 HTTP 请求，网页加载更快，另外还能减小图片大小
    * 缺点：图片制作麻烦，维护麻烦，占用 background 属性，不太适合大图片

32. 找出如下代码中的错误
    ```
    <style>
      div::after：hover {
        opacity: 85%；
        transition: opactiy .3s step(5,end);
      }
      a:visited {
        font-size: 28px;
      }
    </style>
    <div>
      <a href="jd.com”>京东商场<a>
      <a href="mi.com”>小米网<a>
    </div>
    ```

    * 伪元素没有 :hover 伪类，应改为 div:hover::after
    * opacity 的属性值是 0-1 之间的 number
    * steps()
    * 出于隐私原因，:visited 只能设置颜色
    * 不在同一域名下的地址应添加 https://

33. 如下内容渲染在【同一行】中，请计算那一行的理论行高
    ```html
    <!DOCTYPE html>
    <html>
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width">
      <title>JS Bin</title>
      <style>
        div {
          margin: 80px;
          background-color: violet;
        }
        span {
          display: inline-block;
          border: 1px dotted;
          background-color: pink;
        }

        .a {
          vertical-align: -15px;
          width: 30px;
          height: 30px;
        }
        .b {
          margin-top: -50px;
          width: 30px;
          height: 30px;
          vertical-align: top;
        }
        .c {
          margin-bottom: 10px;
          vertical-align: middle;
        }
        .d {
          width: 30px;
          height: 30px;
        }
      </style>
    </head>
    <body>
      <div>
        x<span class="a">foo</span>
        <span class="b">bar</span>
        <span class="c">baz</span>
        <span class="d"></span>
      </div>
    </body>
    </html>
    ```

    * 47px

34. `vertical-align` 取值为 `baseline` 时在不同情况下分别是如何对齐的？

    * inline 元素
      * 非替换元素：字符 x 的下边缘
      * 替换元素：元素自身下边缘
    * inline-block 元素
      * 无内容或不可见：margin 底边缘
      * 有内容：最后一行内联元素的 baseline

35. 解释常规流与包含块的概念

    * 常规流：元素在其中从左往右、从上向下显示的文本布局，浮动和定位元素不在其中
    * 包含块：包含其他盒子的块

36. 在各种情况下，一个元素的包含块分别是什么？

    * 常规和浮动元素的包含块是最近的父元素，定位元素的包含块是最近的定位祖先元素

37. 默写与背景相关的属性并说明每个属性的作用和会产生的效果

    * background-image: 背景图片
    * background-color：背景颜色
    * background-repeat：背景是否重复
    * background-size：背景大小
    * background-position：背景位置
    * background-clip：背景延伸到何处为止
    * background-attachment：背景固定位置
    * background-origin：背景图片原点

38. 如何实现单方向的盒子阴影？

    * 负扩散半径
    * 父元素裁掉其他方向的 box-shadow
      * 父元素 overflow: hidden;
      * 子元素添加单方向 margin

39. 默写与表格布局相关的 CSS 属性，并说明相关属性的作用

    * display：设置 table 或者 table-cell 布局
    * vertical-align：设置垂直文字方向
    * text-align：设置水平文字方向

40. `visibility:hidden`，`display:none`，`opacity:0`分别有什么不同？

    * visibility：元素仍然占据原来的位置（transition没有动画但是有延迟）
    * display：元素不再占据位置
    * opacity：元素变成透明，仍然占据原来的位置，可以配合 transition

41. 当页面中出现表示时间的文字但表意不明确时，比较优雅且富有语义的做法是什么？

    * time 标签

42. CSS 中一般为何不使用 `cm`，`mm` 等长度单位？

    * 其实并不精准，而且不同分辨率表现可能也不一样

43. 表格布局中各层的层次是？

    * cells > rows > row groups > columns > column groups > table

44. 为什么要在文件的最后一行加一个回车？

    * 一行以换行符结尾，不这么做的话在文件拼接时将无法确保文件完整性

45. 用 CSS 画出一个半圆形，分别给出实心与空心的的画法。实心和空心分别给出两种方案。

    * 解答如下：
    ```html
    <style>
      .semi {
        width: 200px;
        height: 100px;
      }
      .semi-2 {
        width: 200px;
        height: 200px;
        overflow: hidden;
      }
    </style>
    <div class="semi"></div>
    <div class="semi-2">
      <div></div>
    </div>
    ```

    ```css
    /* 实心 1 */
    .semi {
      border-radius: 100px 100px 0 0;
      background: aqua;
    }
    /* 实心 2 */
    .semi-2 div {
      width: 100%;
      height: 100%;
      border-radius: 100%;
      background: aqua;
      margin-top: -100px;
    }
    /* 空心 1 */
    .semi {
      border-radius: 100px 100px 0 0;
      border: 1px solid aqua;
    }
    /* 空心 2 */
    .semi-2 {
      border-top: 1px solid aqua;
    }
    .semi-2 div {
      width: 100%;
      height: 100%;
      border-radius: 100%;
      border: 1px solid aqua;
      margin-top: -100px;
      box-sizing: border-box;
    }
    ```

46. 表单元素有哪些伪类选择器？

    * :enabled 匹配表单中激活的元素
    * :disabled 匹配禁用元素
    * :checked 匹配表单中被选中的单复选框
    * :indeterminate 匹配状态不确定的表单元素（比如未选择的单复选框）
    * :invalid 匹配未通过验证的表单元素（input、form）
    * :optional 匹配没有 required 属性的表单
    * :required 匹配必填表单

47. 如何禁用 textarea 元素默认的可缩放行为？

    * resize: none;

48. reset与normalize分别是什么，区别是什么，何种场景会使用到？

    * 均用于重置默认样式，使各浏览器保持统一，但是 reset 为此几乎消除了几乎所有默认样式，而 normalize 在统一浏览器的情况下，保留了默认样式
    * reset 有点过度消除默认样式，结果就是十分占据资源（因为通配选择器），而且有时候还需要改回来；normalize 相对平和，保留了可用的默认样式，并且修复了常见的 PC 、移动端 bug
    * 需要兼容各浏览器，并且不介意冗余代码，或者能手动修改源码时可以使用

49. 表布局中边框合并的原则是什么？

    * 相邻单元格共享边框，并且会基于层次覆盖

50. 如何让背景图片从元素的左下角向上偏移5像素，往右偏移3像素；图片不重复平铺？写出代码。

    * 解答如下：

    ```css
    background-position: bottom 5px left 3px;
    background-repeat: no-repeat;
    ```

51. 写出创建如下目录结果的命令行脚本（注：有扩展名的为文件，没有扩展名的为文件夹）。
    ```
    a
    │  readme.md
    │
    ├─foo
    │  └─c
    └─bar
        │  a.txt
        │  b.txt
        │
        └─y
                a.js
    ```

    * 解答如下：

    ```sh
    mkdir -p a/foo/c a/bar/y
    touch a/readme.md
    cd a/bar
    touch a.txt b.txt y/a.js
    ```

52. 中英互翻

    omit，multiple，驼峰式，中划线式，layout，typo，code review，半径，config，集合，矩形，binary，decimal，十六进制，八进制，SEO，HTML实体，语义化，兼容性，quirk，reference，大小写敏感，别名

    * 删除，多重，Camel-Case，hyphen delimiters，布局，排版错误，代码审查，radius，配置，collection，rrectangle，二进制，十进制，hex，octal，搜索引擎优化，Character Entity，semantic，compatibility，怪异（quirks mode），参考资料，case sensitivity，alias


12
17
26
27
33
34
35
39