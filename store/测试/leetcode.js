// 翻转链表
var reverseList = function (head) {
  let node = head
  let prev = null
  let next = null
  while (node.next) {
    next = node.next
    node.next = prev
    prev = node
    node = next
  }
  return node
}

// 深拷贝
function deepClone (value) {
  const parents = []
  const children = []
  return clone(value)
  
  function clone (value) {
    if (!value || typeof value !== 'object') return value
  
    const ctor = value.constructor
    let obj
  
    switch (ctor) {
      case RegExp:
        obj = new ctor(value)
        break;
      case Date:
        obj = new ctor(value.getTime())
        break;
      default:
        obj = new ctor()
    }

    // 处理循环引用
    const index = parents.indexOf(value)
    if (index !== -1) {
      return children[index]
    }
    parents.push(value)
    children.push(obj)
  
    for (let key in value) {
      if (value.hasOwnProperty(key)) {
        obj[key] = clone(value[key])
      }
    }

    return obj
  }
}
