大喵教育前端培训
================

## 阶段性测试 2018.08.24

### 大喵教育版权所有 | 出题人：谢然

01. 读程序写结果
    ```js
    var ary = [1,function(){console.log(this[0])}]
    console.log(ary[1]())
    ```

    - 1, undefined

02. 读程序写结果
    ```
    var obj = {
      a: 3,
      b: this.a * this.a
    }
    console.log(obj.b)
    ```

    - NaN
    * 这时候 this 还是 undefined ，因为赋值还没有完成

03. 如何判断一个函数当前是否被当做构造函数调用？

    - es6: new.target
    - es5: this instanceof constructor

04. 什么叫做排序算法的稳定性？它有什么作用？

    - 排序后是否会打乱相等的值的位置
    * 稳定的：冒泡，归并，插入-BST
    * 不稳定的：快排，选择，堆排序
    - 可以实现多条件按优先级排序（多级排序）

05. 严格模式是什么？如何触发？有哪些限制？

    - 一种更严格的 js 运行环境
    - 'use strict'
    - 限制：
        - 必须事先声明变量
        - 未指定的 this 以及直接调用函数时的 this 会变成 undefined
        - 函数不能有相同的参数
        - 不能使用 with ，eval 将只能给当前作用域声明变量
        - 不支持 arguments.callee 和 function.caller
        - 在条件、循环语句中无法声明函数

06. try catch finally 语句一般在什么情况下使用？有哪些需要注意的问题？最佳实践是什么？

    - 在需要执行可能会抛出异常的语句时使用
    * 只能捕获运行时错误，不能捕获语法错误（无法通过语法分析，不会开始运行）
    - 使用 instanceof 确定 catch 到的异常的类型，确认是否是自己预期的，如果不是则继续抛出

07. 简述你对面向对象的三大特性的理解（不是把三大特性列出来）

    - 封装：对象会有自己的属性和方法，可以用来获取对象的数据或者操作对象
    - 继承：对象可以继承自另一个对象，这样就能拥有父类的属性和方法
    - 多态：同一个方法，调用它的是不同的实例，就能获得不同的结果，比如 toString
    * 往往我们只需要事物某一方面的特性，此时只要给定的事物具有这个方面的特性，我们逻辑就能够走通。具体到程序中，就是只要某一个对象有特定的一组属性及方法即可，不需要它是特定类型的。

08. 解释一下你对构造函数的理解

    - 用来创建某一类型的实例和原型，实例可以获取到原型上的属性和方法
    * 通过给定的参数构造一个特定类型的对象

09. 如下代码的运行结果是什么？为什么？
    ```js
    var a = 8
    try {
        console.log(
    }
    ```

    - SyntaxError
    - 在解析时碰到 ( 之后没有碰到 ) ，反而碰到了 } ，所以报出语法错误 

10. 使用js语言实现如下功能
    ```js
    //传入表示fullName的字符串，包含两个单词
    var damiao = new Person('Xie Ran')

    console.log(damiao.firstName)//Xie
    console.log(damiao.lastName)//Ran

    damiao.fullName = 'Da Miao'

    console.log(damiao.firstName)//Da
    console.log(damiao.lastName)//Miao

    damiao.firstName = 'Xiao'

    console.log(damiao.fullName)// Xiao Miao
    ```

    ```js
    class Person {
        constructor (name) {
            this.fullName = name
        }

        get firstName () {
            return this.fullName.slice(0, this.fullName.indexOf(' '))
        }

        set firstName (name) {
            this.fullName = name + ' ' + this.lastName
        }

        get lastName () {
            return this.fullName.slice(this.fullName.indexOf(' ') + 1)
        }

        set lastName (name) {
            this.fullName = this.firstName + ' ' + name
        }
    }
    ```

11. 如何删除一个对象的某个属性？如何阻止一个对象的某个属性被删除？

    - delete obj.prop
    - 使用 Object.defineProperty 将属性的 configurable 定为 false
    * Object.freeze(obj)
    * Object.preventExtension(obj)
    * Object.seal(obj)

12. DOM结点的各种属性（property）和`get/setAttribute`有什么区别，联系和坑？

    - attribute 是 HTML 标签上的真正存在的属性，可以通过 get/setAttribute 访问和设置，property 是 DOM 对象的属性，但是也可以用来设置和访问部分（非自定义） HTML 标签属性
    - node.style 只能访问到节点的内联属性和通过 DOM 操作设置的属性，无法访问到样式表中设置的属性，也无法计算真正呈现结果，这时候需要使用 getComputedStyle
    * 有同步、单向同步、完全无关三种情况

13. 为什么把一个DOM中已存在的结点插入到另一个位置会导致它在原来的位置自动消失？

    - 实际上只是移动了位置?

14. 使用var与使用let声明变量有什么区别？

    - let 声明会让 {} 内变成块级作用域
    - 使用 let 不能重复声明
    - 使用 let 会有临时性死区

15. 请分析快速排序算法的复杂度为；另分析使用快排变形算法找出乱序数组中第k大的数的复杂度

    * 2n

16. 如下程序以【何种方式】输出【何种结果】？
    ```js
    for(var i = 0; i<5; i++) {
      let j = i
      setTimeout(function(){
        console.log(j)
      }, 1000)
    }
    ```

    - 执行完 for 循环之后依次执行任务队列中的回调函数，输出 0 1 2 3 4

17. 至少列出四种this在不同情况分别指向

    - 在全局环境中时指向 window
    - 直接调用函数时，指向 window ，严格模式中是 undefined
    - 作为对象的方法调用时指向对象本身
    - 在构造函数中指向实例
    - 使用 call bind apply 时指向第一个参数
    - 箭头函数中指向函数外部作用域中的 this
    - getter/setter的this指向调用时的对象

18. 如何给一个已存在的对象添加getter/setter属性，写出代码。

    ```js
    Object.defineProperty(obj, prop, {
        get: function () {},
        set: function () {}
    })
    ```

19. 实现lodash的`forOwn`函数，用于遍历一个对象的自有属性

    ```js
    function forOwn (object, iteratee) {
        Object.entries(object).forEach(([key, value]) => iteratee(key, value, object))
    }
    ```

20. 实现一个类【Quene】用以表达一个队列：
  * 队列是一种类似于数组的数据结构，但元素仅能从一边进入且仅能从另一边弹出队列，类似火车站
  * 设计良好的构造函数及接口以方便自己和他人使用
  * 至少需要实现的几个实例方法（其它方法按需实现）：
    * `Quene.prototype.next` 获取队列的下一个元素
    * `Quene.prototype.push` 将一个元素放进队列
    * `Quene.prorotype.length` 获取队列的长度（getter）

    * 使用数组的话每次都需要调整数组，其实也有O(n)的消耗
    * 队列最好使用链表实现
        * 两个指针，一个指向末尾，一个指向头部，新数据添加到末尾，获取数据从头部开始

21. prototype与`__proto__`有什么区别和联系？

    - 每个函数都有 prototype 属性，prototype 有一个 constructor 属性指向函数本身
    - 每个对象都有 `__proto__` 指向对象的原型的 prototype 属性（只有 Object.prototype 的 `__proto__` 是 null）

22. 影响排序算法性能的本质原因是什么？

    * 排序其实是在改变原数组的逆序数，排序算法多数时候是在交换数组中元素的位置
    * 所以影响效率的最大因素即为每一次交换对逆序数的改变

23. 如下数组按照经典算法就地调整成一个堆后的结果是什么样？
    [3,5,1,1,2,8,9,0,7,4]

    - [0,1,1,3,2,8,9,5,7,4]

24. 使用 new 调用一个函数时具体发生了什么？

    - 创建一个空对象，将this指向这个对象，将对象的`__proto__`指向函数的prototype
    - 执行函数中的内容
    - 如果执行结果是一个对象则返回它，否则返回之前创建的对象

    ```
    function NEW(f,...args) {
        var obj = {}
        obj.__proto__ = f.prototype
        var ret = f.call(obj,...args)
        if (ret && typeof ret === 'object') {
            return ret
        } else {
            return obj
        }
    }
    ```

25. 什么叫做断言？它有什么用？一般用在什么地方？

    - assert
    - 指某段程序的返回结果一定是某个值，如果不是的话说明程序出错
    - 用在测试函数中
    * 写成一个断言函数是为了不必在每个需要检测参数的位置手动if+throw判断

26. DOM结点的innerHTML，innerText，textContent，outerHTML，outerText分别有什么区别？

    - innerHTML：不包括节点本身的html字符串（会覆盖原来的，包括事件也会被清空）
    - outerHTML：包括节点本身的html字符串（会受css影响）
    - textContent：不会受css影响，完全是元素内部所有文本结果的字符串拼接
    - innerText：不包括节点本身的文本字符串
    - outerText：包括节点本身的文本字符串

27. 什么是属性描述符？如何获取一个对象某属性的属性描述符？

    - 指一些表述属性特征的配置，比如是否可修改、是否可遍历等
    - Object.getOwnPropertyDescriptor(object, prop)

28. 在去除癌症细胞一题中，为什么在正则的分支中，小c写前面也可以通过测试？
    ```js
    //即如下代码中，正则在替换掉第一个小c后，在继续往下扫描的过程中，为何没有替换掉xC？
    'abAaBxcCbcabc'.replace(/(c|[a-z]?C[a-z]?)/g, '')
    ```

    - 判断完之后不会再回头判断

29. 用正则表达式将以下字符串中不在双引号内的逗号变为“|”。
    ```js
    'foo,bar,baz,"damiao,xiaomiao",a,"b,c",d'
    ```

    - 'foo,bar,baz,"damiao,xiaomiao",a,"b,c",d'.replace(/,(?=([^"]*"[^"]*")*[^"]*$)/g, '|')

30. 补全如下replace函数的调用使其将如下字符串中左边不是xx但右边是yy的OO替换为XX.
    ```js
    'lskOOyydjfxxOOyyiwexxOOhayyOOxxlskdwOOyyflxxOOyykaxOOydfh'.replace()
    ```

    - 'lskOOyydjfxxOOyyiwexxOOhayyOOxxlskdwOOyyflxxOOyykaxOOydfh'.replace(/(?<!xx)OO(?=yy)/g, 'XX')
    * (?!xx)..OO(?=yy) （用右零宽断言来实现左零宽断言）

31. 画出以下正则表达式的铁路图
    ```js
    /^(-)?\d+(.\d{1,2})?$/
    ```
32. 写出与如下铁路图等价的正则表达式
    ![](reg-railway.png)

    - /^#?([a-f0-9]{0,6}|[a-f0-9]{0,3})$/

33. 双击以下代码渲染结果中一段话，会在控制台输出什么？并给出解释
    ```html
    <div>
      <p>Lorem ipsum dolor sit amet.</p>
    </div>
    <div>
      <p>dolor sit amet.</p>
    </div>
    <script>
      $('body').find('div').click(function(e) {
        console.log(e.target.tagName)
      }).dblclick(function(e) {
        console.log('div got double clicked')
      }).find('p').dblclick(function(e) {
        console.log(this.innerText)
        e.stopPropagation()
      }).end().end()
    </script>
    ```

    - 'p', 'p', 'Lorem ipsum dolor sit amet.'
    - dblclick 之前先触发了 click 事件，冒泡到父元素上执行了回调；dblclick 事件被阻止冒泡，没有冒泡到父元素上

34. 如下代码输出什么，解释原因并说明如何规避这种情况。
    ```js
    console.log(new Array(8).map((v,i) => i))
    ```

    - [empty * 8]
    - 因为 map 不会遍历数组中的 空洞，可以先将数组填满再用 map
    - console.log(new Array(8).fill(1).map((v, i) => i))

35. 说出无限滚动页面（类似花瓣、知乎，滚动到底部就继续加载）的实现思路，并写出大致伪代码

    - 当滚动到页面底部时，将下面的内容 append 到容器中

    ```js
    var container = document.querySelector('.container')

    window.addEventListener('scroll', function () {
        if (document.documentElement.scrollTop >= window.pageYOffset - 20) {
            loadChild()
        }
    })

    function loadChild () {
        var child = getChild()
        container.appendChild(child)
    }
    ```

36. 哪些事件默认不会冒泡？

    - resize、mouseenter、mouseleave、focus、blur、onload、onerror

37. 写出优化大型页面滚动过程的代码：
    * 将不在视口区域内的内容从DOM中移除
    * 将在视口区域的内容显示出来
    * 但是不能让它们的隐藏或显示影响页面布局
    * 具体的隐藏方式可任意

    - 

38. 什么是多重转义，在什么时候会用到？使用合适的语言描述出来。

    - 每次需要转换时，由于某些字符可能引起歧义，需要每次都转义一次
    - 比如在正则表达式中需要用到转义符号时，定义正则表达式的时候还需要转义一次：`var reg = new RegExp('\\n')`
    * 当用一种语言表达另一种需要转义的内容时，可能涉及到多重转义

39. 什么是正则表达式中的零宽断言？并举出至少两个使用场景。

    - 用于查找某些内容但是结果中又不包括这些内容
    - 找到后面是 '-' 的字符串，找到不包含某个字符的字符串

40. 分别说出以下几个与正则相关的函数的【各种】用法及效果，可以查文档
    * String.prototype.replace
    * String.prototype.search
    * String.prototype.match
    * RegExp.prototype.test
    * RegExp.prototype.exec
41. HTML、JS、正则表达式、URL encode、CSS等的转义符分别是什么

    ```
    HTML:
    &xxx;

    JS：
    \
    \u{}
    \u6211

    正则：同js

    URL编码：
    %

    CSS：
    \A
    ```
42. 什么是vanilla.js？

    - ...

43. 为字符串对象增加一个方法replaceAll（wildcard，target），讨论poilfill
    * 其中wildcard为一个表示通配符的字符串
    * target可以为一个字符串，也可以为一个函数
        * 如果是一个字符串，则把所有匹配到的内容替换为该字符串
        * 如果为一个函数，则把每一个匹配得到的内容传给该函数，并将该函数的返回值做为被替换的内容
    ```
    能完全在旧版本浏览器上实现新功能：
    polyfill
    conic-gradient
    大多数的函数：
        Array.of
        Array.prototype.includes
        String.prototype.startsWith

    不能完全实现但至少不报错：
    shim  shiv
        console.log
        html5shim.js
    ```

44. 描述正则表达式的匹配过程

    - 回溯

45. 如何阻止事件冒泡？如何阻止浏览器对某些事件的默认行为？如果模拟用户的点击事件？

    - e.stopPropagation
    - e.preventDefault

    * https://developer.mozilla.org/en-US/docs/Web/Guide/Events/Creating_and_triggering_events

46. 什么是事件代理？

    - 将事件处理函数绑定给父元素，触发事件会冒泡到父元素上并执行回调函数，这样就不需要给每个子元素都绑定一个事件

47. 给元素绑定事件处理函数有多少种方式？每种方式各有什么优缺点？

    - 在 HTML 中写入 onclick
    - node.onclick
    - 以上两种只能绑定一个函数而且只能注册冒泡
    - node.addEventListener：可以绑定多个回调函数，并且可以注册捕获

48. 如何阻止事件的默认行为？哪些事件的默认行为无法阻止？应对方案是什么？

    - e.preventDefault()
    * scroll onclose onbeforeunload
    * 只能换其他类似事件

49. mouseover与mouseout事件有什么需要注意的问题？

    * 会冒泡
    - 前者会连续触发，需要函数节流或者防抖根据场合而定
    - 后者在鼠标进入子元素时会触发父元素的 mouseout

50. debounce与trottle函数的适用场景有什么不同？

    - debounce 函数只要在一定时间内连续触发就不会调用（只需要在最后一次调用）
    - trottle 函数在离上一次调用未超过一定间隔前不会调用（降低调用频率）
    - 比如连续的 resize ，trottle 每过一定时间下一次必定会调用，debounce 只有在停下来之后下一次才会调用

51. 说出尽量多不那么常见的事件

    - online、abort、animationend、submit、fullscreenchange、copy、contextmenu、drag、loadstart、progress

52. 什么叫token？将如下代码中的token写成一个js数组
    ```js
    const {Readable, Writable} = require('stream')
    while(true) {
      console.log(1)
    }
    ```

    - 词法分析后获得的构成代码的单位，之后传给解析器解析
    - [const, {, Readable, \,, Writable, }, =, require, (, ', stream, ', ), while, (, true, ), {, console, ., log, (, 1, ), }]
