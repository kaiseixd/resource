https://react.docschina.org/blog/2015/12/18/react-components-elements-and-instances.html
https://tech.youzan.com/react-fiber/
https://segmentfault.com/a/1190000012834204
http://imweb.io/topic/57711e37f0a5487b05f325b5
https://www.kancloud.cn/kancloud/react-in-depth/47778
https://github.com/reactjs/react-basic
https://www.cnblogs.com/hhhyaaon/p/5863408.html
https://github.com/ckinmind/ReactCollect/issues/20
https://juejin.im/post/596d65d66fb9a06bae1e19e2
https://juejin.im/entry/59a4eca4f265da24951798a5

**设计理念**
  view
    react解决的是view层上的问题，关注的是如何高效地将服务端或用户输入的动态数据反映到复杂的用户界面上（主要是免去了频繁的DOM操作），它是一个用来构建用户界面的库
  virtual dom
    对数据渲染进行优化
  JSX
    使用XML标记的方式直接去声明界面
  组件化
    封装起来的具有独立功能的UI部件，对于重用、维护、测试来说都有好处
  单向数据流：Flux
    Dispatcher是一个全局的分发器负责接收Action，而Store可以在Dispatcher上监听到Action并做出相应的操作
    用户界面操作或服务端数据更新导致数据模型发生变化，触发界面更新
  只读数据：immutable.js
    让React组件能够仅通过对象引用来判断是否需要重新render

**jsx**
  概念
    React使用JSX来描述用户界面，在babel转译后，元素会被转换成普通的js对象（使用createElement），以给virtual dom使用
    本质上来讲，JSX 只是为 React.createElement(component, props, ...children) 方法提供的语法糖
  使用原因
    与模板语言相比来说不需要学习新的语法，在js里写xml（构建界面）可以利用js语法和特性直接解决上下文绑定等问题

**props**
  只读
  render props
    指一种在组件之间使用一个值为函数的 prop 在 React 组件间共享代码的简单技术

**组合**
  对于无法预知子组件的组件，可以使用props.children属性将子元素直接传递到输出，当然也可以自己约定属性

**生命周期**

old
  挂载
    constructor -> componentWillMount -> render -> componentDidMount
  更新
    props change -> componentWillReceiveProps -> shouldComponentUpdate -> componentWillUpdate -> render -> componentDidUpdate

    state change -> shouldComponentUpdate -> componentWillUpdate -> render -> componentDidUpdate
  卸载
    componentWillUnmount

new(16.3)
  挂载
    constructor -> getDerivedStateFromProps -> render -> componentDidMount
  更新
    props or state change -> getDerivedStateFromProps -> shouldComponentUpdate -> render -> getSnapShotBeforeUpdate -> componentDidUpdate
  卸载
    componentWillUnmount

**setState**
  自动调用render
  状态更新合并
    react会将多个setState调用合并成一个以提高性能
    可以给它传一个函数来解决这个问题：(prevState, props) => ({ func body })
  异步更新
    setState是异步更新的

**组件**
  元素组件
  函数组件和类组件
    函数组件
      性能更佳
    类组件
      局部状态、生命周期钩子

  展示与容器组件 Presentational & Container
  有状态与无状态组件 Stateful & Stateless
  受控与非受控组件 Controlled & Uncontrolled
    受控组件
      值由React控制的输入表单元素
      需要为数据可能发生变化的每一种方式都编写一个事件处理程序，并通过一个组件来管理全部的状态
    非受控组件
      由DOM操作处理表单
      一般是由ref拿到dom元素

  组合与继承 Composition & Inheritance

**ref**
  适合
    处理焦点、文本选择或媒体控制，触发强制动画，集成第三方 DOM 库
  你不能在函数式组件上使用 ref 属性，因为它们没有实例
  ref转发
  回调ref（在回调函数里传element）
  传递ref
    React.forwardRef

**性能优化**
  避免重复渲染
    scu
    PureComponent
      自动在scu里做浅比较来判断组件是否需要更新（配合immutable.js避免突变）

**createReactClass**
  代替class写法(ES6)
  方法可以自动绑定到实例，不需要bind
  支持Mixin(生命周期、方法)

**createElement**
  代替JSX写法

**context**
  概念
    通过组件树提供了一个传递数据的方法，从而避免了在每一个层级手动的传递 props 属性
  使用场合
    多个层级的多个组件需要访问相同数据的情景
  API
    React.createContext
      创建一对Provider、Consumer，Consumer从组件树上层最近的匹配的Provider中读取当前context值
    Provider
      提供context
      接收一个 value 属性传递给 Provider 的后代 Consumers
    Consumer
      订阅context变化的子组件
      接收一个 函数作为子节点，函数接收当前 context 的值并返回一个 React 节点
    tip
      每当Provider的值发送改变时, 作为Provider后代的所有Consumers都会重新渲染。 从Provider到其后代的Consumers传播不受shouldComponentUpdate方法的约束
      想让子组件能改变context，可以传一个函数

**Fragment**
  <></> 是 <React.Fragment/> 的语法糖

**Portals**
  概念
    可以将子节点渲染到父组件以外的 DOM 节点
  适合
    dialog等不希望受父组件overflow、position、z-index影响的组件
  用法
    render() {
      return ReactDOM.createPortal(child, container)
    }

**Error Boundaries**
  概念
    用于捕获其子组件树 JavaScript 异常，记录错误并展示一个回退的 UI 的 React 组件，而不是整个组件树的异常
  componentDidCatch(err, info)

**组件通信**
  父子 props
  子父 事件
  兄弟
    状态提升
    context
    观察订阅（EventEmitter）
    redux flux

**fiber**
  核心理念
    在UI中，并非所有更新都需要立即生效，这样可能会浪费资源引起掉帧，像界面互动动画的优先级就应该比数据更新的优先级高。React以“拉”为基础决定调度工作，由框架自行决定更新的优先级和顺序
  概念
    对 virtual dom 进行了重写（当然diff算法还是相似的），使 virtual dom 能够进行增量渲染：能够将渲染 work 拆分成多块并将这些任务块分散到多个帧执行
  作用
    提高其对动画，布局和手势等领域的适用性（防止掉帧）
  核心特性
    将work拆分成多块分散到多个帧执行
    当新的更新到来时暂停、中止或恢复 work
    为不同类型的更新设置优先级
    新的并发原语（?）
  实现目标
    重新实现了一个虚拟栈帧，用来自定义调用栈行为（随意中断调用栈或手动操作栈帧）
  结构
    是一个包含了组件及其输入输出的 JavaScript 对象，对应于一个栈帧，但是它也对应与一个组件的实例
    type & key
      与React元素中的作用相同，fiber的type描述了对应的组件，对于复合组件这是函数或类本身，对于宿主环境的组件（div、span 等）则是字符串
      key则决定协调期间fiber是否可以重新使用
    child & sibling
      指向了其他fibers，描述了 fiber 构成的递归结构的树
      child fiber 对应于组件 render 方法返回的值
      sibling 字段是为了解释 render 方法返回多个子元素的情况
    return
      程序处理完当前 fiber 后应该返回 fiber，返回的 fiber 在概念上等同于返回栈帧的地址
      child fiber 应该是由 parent fiber 返回
    pendingProps & memoizedProps
      pendingProps 是 fiber 执行开始时的属性集合，memoizedProps 则是 fiber 执行结束时的属性集合
      下一个 pendingProps 等于 memoizedProps，这样就可以重用之前 fiber 的输出
    pendingWorkPriority
      一个用来表示 fiber 代表的任务优先级的数字
    alternate
      一个组件实例至多对应两个fibers，分别是已flush的或正work-in-progress的，两个fiber相互交替
      交替过程使用cloneFiber实现，将尝试去重复利用已存在的 fiber，来最小化分配
      flush
        flush 一个 fiber 就是把它的输出应用到屏幕上
      work-in-progress
        一个 fiber 还没有完成；从概念上讲，就是一个栈帧还没有返回
    output
      fiber 输出的是函数的返回值
      每一个 fiber 最终都有输出，但是输出只包含 host components 叶子节点，输出的内容随后将被转移到树上
      输出的内容最终会给到渲染器，以至于改变能够被应用到真正的渲染环境上，输出如何被创建和更新则是渲染器的职责
      host component
        React 应用的叶子节点，由特定的渲染环境决定

  调度器如何找到下一个任务单元去执行
  优先级在 fiber 树结构中是如何被跟踪和传播的
  调度器如何知道什么时候去暂停和恢复任务
  任务如何被刷新和标记为完成
  副作用（如生命周期方法）如何工作
  什么是协程以及如何利用它来实现上下文和布局的特性

  requestIdleCallback
    在浏览器处于闲置状态时调度一个低优先级的函数去执行
  requestAnimationFrame

**virtual dom**
  核心理念
    React的数据更新会对整个应用进行重新渲染，这样开发者就不需要考虑具体的状态过渡
  解决性能问题
    重新渲染肯定会有效率问题，所以React对数据渲染使用virtual dom进行了优化，用js实现DOM API，数据变化时会将整个DOM树和上一次DOM树对比，得到区别，仅对变化部分进行DOM更新；并且能够批处理DOM更新，在一个event loop里的数据变化会被合并（虽然效率不一定是最佳的，但是免去了手动操作DOM）
  更加高层的描述
    当渲染一个 React 应用，会生成一棵描述应用结构的节点树，并保存在内存中。这棵树随后被刷新到渲染环境。举例来说，我们的浏览器环境，它会转换为一个 DOM 操作的集合。当应用更新的时候（通常是通过 setState 触发），会生成一棵新的树。新树会和先前的树进行对比，并计算出更新应用所需要的操作

**reconciliation(diff)**
  virtual dom背后的算法

**Scheduling**
  scheduling
    决定 work 何时应该被执行的过程
  work
    任何计算结果都必须被执行。Work 通常是更新的结果(e.g. setState)

**hook**

**redux**
  流程
    （用户界面操作或者服务端数据变更）调用store.dispatch发出action
    store调用reducer并返回新的state
    state变化触发监听函数，进行视图更新
      store.subscibe(listener)

    react-redux的话，通过connect可以直接把state和dispatch绑到props上，就可以自动触发更新

  实现
    createStore(reducer, preloadedState, enhancer)
      currentReducer = reducer
      currentState = preloadedState
      currentListeners
      nextListeners
        保证每次遍历监听器时currentListeners数组大小不变，索引不会出错
      return
        dispatch(action)
          执行combineReducers组合后的reducers函数
          return action
        subscribe(listener)
          nextListeners.push(listener)
        getState
          return currentState
      summary：
        接收reducer和默认state，返回dispatch和getState和subscribe等闭包函数
        这样调用dispatch的时候就可以访问到reducer，并且也能在里面访问到currentState（getState的返回值）

    combineReducers(reducers)
      reducerKeys
        Object.keys(reducers)
      finalReducers
        过滤后的reducers (typeof should be 'function')
      finalReducerKeys
        Object.keys(finalReducers)
      assertReducerShape(finalReducers)
      return function combination(state, action)
        combineReducers的结果，是一个返回整合后reducer的函数，参数为state和action
        创建一个nextState对象作为函数返回结果
        遍历finalReducerKeys
          previousStateForKey = state[key]，因为reducer和state具有同样的key
          nextState[key] = reducer(previousStateForKey, action)
      summary
        接收reducers对象，过滤后遍历它
        返回一个整合了所有reducer的函数，传入state和action就能调用所有reduce并返回新state

    applyMiddleware(...middlewares)
      store = createStore(...args)
      middlewareAPI = { getState: ..., dispatch: ... }
      chain = middlewares.map(middleware => middleware(middlewareAPI))
        调用每个middleware并传入middlewareAPI（只是为了让middleware拿到store API，返回还是函数：next => action）
      dispatch = compose(...chain)(store.dispatch)
      return { ...store, dispatch }
        返回加上了中间件的dispatch函数(composed dispatch)和剩余的store属性
      summary
        返回一个柯里化函数（applyMiddleware(...middlewares)(createStore)(...args)）
          调用每个middleware并传入store API
          用compose组合出一个reduce函数，先调用store.dispatch再调用中间件函数
          返回store和组合后的dispatch

    compose(...funcs)
      return funcs.reduce((a, b) => (...args) => a(b(...args)))
      summary
        让函数从右到左嵌套调用

    function myMiddleware = store => next => action => { }

    function createThunkMiddleware(extraArgument) {
      return ({ dispatch, getState }) => next => action => {
        if (typeof action === 'function') {
        // 是函数的话再把这些参数传进去，直到 action 不为函数，执行 dispatch({tyep: 'XXX'})
          return action(dispatch, getState, extraArgument);
        }
        return next(action);
      };
    }

**react-redux**
  <Provider store>
    使组件层级中的 connect() 方法都能够获得 Redux store
    不想让根组件嵌套在Provider中的话可以把store作为props传递到每个被connect包装的组件
  connect
    HOC，连接react组件与redux store，返回一个连接好(state dispatch -> props)的组件
    mapStateToProps
      组件将监听store变化，并在关联store改变时调用
      ownProps：该参数的值为传递到组件的 props，而且 props 改变时就会调用函数
      （最好在多个组件上使用 connect()，每个组件只监听它所关联的部分 state）
    mapDispatchToProps
      dispatch
        如果传入的是对象，则每个属性对应方法
        如果传入的是函数，则参数需要接受dispatch，并返回一个对象
      ownprops
      如果省略了mapDispatchToProps参数，默认情况下dispatch会被注入组件props中
    mergeProps
      接收mapStateToProps和mapDispatchToProps和props作为参数返回一个对象作为组件props

**cra配置**

**参考**
  React官方文档
  https://www.kancloud.cn/kancloud/react-in-depth/47779
  https://github.com/xxn520/react-fiber-architecture-cn