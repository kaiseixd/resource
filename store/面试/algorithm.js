class ListNode {
  constructor(val, next) {
    this.val = val;
    this.next = next;
  }

  addAtTail(val) {
    let node = this;
    while (node.next) {
      node = node.next;
    }
    node.next = new ListNode(val);
  }
}

var a = new ListNode(1);
a.addAtTail(2);
a.addAtTail(3);
a.addAtTail(4);
a.addAtTail(5);
