# node

- process.argv
- process.execArgv
- --inspect-brk

## fs
### createReadStream/createWriteStream

创建一个将文件内容读取为流数据的ReadStream对象，这个方法主要目的就是把数据读入到流中，得到是可读流，方便以流进行操作。

### readFile/writeFile

会将文件内容视为一个整体，为其分配缓存区并且一次性将文件内容读/写取到缓存区中。

### read/write

不断地将文件中的一小块内容读/写入缓存区，最后从该缓存区中读取文件内容。

# DOM
## 方法

- Element.normalize：将 text node 合并
- querySelectorAll
  - 无法选择伪元素（部分伪类是可以的），因为伪元素对 js 是透明的
  - 返回静态集合，不会动态更新
- attribute 和 property 的区别
- js 动态修改 dom 节点的 css 时，浏览器会等 js 执行完再修改布局（如果期间有 js 代码需要读取布局相关的数据，浏览器也会计算但是还不会修改布局）

# 堆（heap、优先队列）
## 213

能够以高效的方式维护集合的最大值或者最小值，是一种特殊形式的二叉树（对于最小堆，每个根节点都比左右子树小）

# RegExp

[http://deerchao.net/tutorials/regex/regex.htm]

## 语法
### 声明

```js
let reg = /\d{4, 6}/

// 遇到 '\' 需要转义
let reg = new RegExp('\\d{4, 6}')
let reg = new RegExp(String.raw `\d{4, 6}`)

// 第二个参数可以覆盖 reg.flags
new RegExp(/abc/ig, 'i').flags  // 'i'
```

### 修饰符

- g
- i
- u：支持 utf-16
- y：黏连符，每次匹配必须在当前 index（lastIndex） 的开头
  - exec，replace?，split?
- m
- s：dotAll
  - . 可以匹配行终止符：换行符、回车符、行分隔符、段分隔符

### 方法

- reg.test：return true of false
- reg.exec：找到正则匹配到的项以及每个括号匹配到的项，括号如果没匹配到内容会是 - undefined（在这个括号内 {0, n} 时）
- reg.escape：转义字符串为正则构造函数可用

- str.match：基本类似 exec ，但是在正则后加上 g 可以匹配到所有的项（exec 还是匹配不了所有的项，因为是一个一个 index 匹配的），返回数组
- str.split：
  - 'foo bar'.split(/(.)\1/) 的时候，分组捕获到的内容也会插入到结果中 -> 'f' 'o' ' bar'，这种时候可以选择非捕获分组：/(?:.)\1/
- str.replace：
  - 'AB'.replace(/(.)(.)/g, '$2$1($&)')
  - 还可以是 function
- str.search

### 属性

- reg.lastIndex
  - exec 执行一次之后，lastIndex 就会变成匹配到的 index 值
  - 可以改变 exec 开始的位置（有 g 的时候总是从 0 开始）
  - test 也会受影响
- reg.source
- reg.global
- reg.flags

### \b 匹配单词的边界

- `/\bhi\b/.test('a, hi')`

### ^ 匹配字符串的开始

### $ 匹配字符串的结束

### . 匹配除换行符以外的任何单个字符

- /.n/将会匹配 "nay, an apple is on the tree" 中的 'an' 和 'on'，但是不会匹配 'nay'

### * 匹配前一个表达式0次或多次。等价于 {0,}

- 指定*前边的内容可以连续重复使用任意次以使整个表达式得到匹配

### + 匹配前一个表达式1次或更多 {1,}

### ? 匹配前一个表达式0次或1次

### \d 匹配一个数字 [0-9]

- `0\d\d-\d\d\d\d\d\d\d\d`

### \s 匹配任意空白符

- 包括空格，制表符(Tab)，换行符，中文全角空格等

### \w 匹配字母或数字或下划线或汉字等

- 字符?

### {n} 匹配前一个表达式刚好发生了n次

- {n, m} 至少n次，至多m次

### [xyz] 一个字符集合，匹配方括号中的任意字符

- 可以使用 - 来指定字符范围
- 在这里就不需要用 \ 来转义了，相反 \ 也会被当做要匹配的字符

### [^xyz] 反向字符集，匹配不包含在方括号中的字符

### 分支条件 x|y，匹配 x 或者 y

### 分组/子表达式 (xy) 

- 可以用于重复多个字符
- 获取：(.)(.)\2\1 （abba）

### 非捕获分组

- `(?:exp)`
  - exec 的结果中不包含括号中的内容

### 零宽断言

- `(?=exp)`：匹配exp后面的位置
- `(?!exp)`：匹配后面不是exp的位置
- `(?<=exp)`：匹配exp前面的位置
- `(?<!exp)`：匹配前面不是exp的位置

### 贪婪与懒惰

- `*`：匹配尽量多的字符
- `*?`：重复任意次，匹配尽量少的字符
- `+?`：重复1次以上，匹配尽量少的字符
- `??`：重复0次或1次，匹配尽量少的字符
- `{n,m}?`：n-m次，尽量少
- `{n,}?`：n次以上，尽量少

## 组合
### .* 匹配任意数量的不换行字符

- `/\bhi\b.*\bLucy\b/`

## 例子

- [https://alf.nu/RegexGolf#accesstoken=Bf7caGITVXKeGVbsp79l]
- [^] 表示任意符号（包括换行）
- ^(?!.*(.)(.)\2\1) 不匹配 abba

```js
// 判断是否是 4 或 6 位数字
/^(\d{4}(\d{2})?$)/.test(pin);

// 替换 'dasdwddwadxdhkjw' 为 '############hkjw'
cc.replace(/.(?!.{0,3}?$)/g, '#')
cc.replace(/.(?=....)/g, '#')

// 去除一句话末尾的 '!!'
s.replace(/\b!+/g, '')
s.replace(/(\w)!+/g, '$1')
s.split(' ').map(char => char.replace(/!+$/, '')).join(' ')

// 去除 C 旁边的字母（大写的不能去除），和 c
organism.replace(/[a-z]?C[a-z]?|c/g, '') // 主要是 ? 的用法

// trim
String.prototype.trim = function(c = ' '){
  return this.replace(new RegExp(`^${c}+|${c}+$`, 'gi'), '')
}

// 简单的 markdown to html
// aaaawawwawwaaaaaaaaooooooeee
function format(string) {
  return (/^(\*[^*])|#/.test(string) ? string : `< p>${string}< /p>`)
  .replace(/\*\*(.+?)\*\*/g, (_, content) => 
    `< strong>${content}< /strong>`)
  .replace(/(#{1,6})(.+)/g, (_, hash, content) =>
    `< h${hash.length}>${content.trim()}< /h${hash.length}>`)
  .replace(/^\* (.+)/g, (_, content) => 
    `< li>${content}< /li>`);
}

// header 的处理很好
function format(s) {
  if (s[0] == '#') {
    let header = Math.min(s.match(/^#+/)[0].length, 6)
    s = s.slice(header).trim()
    s = `< h${header}>${s}< /h${header}>`
  }
  else if (s.slice(0, 2) == '* ') {
    s = `< li>${s.slice(2).trim()}< /li>`
  }
  else s = `< p>${s.trim()}< /p>`
  s = s.replace(/\*\*(.+?)\*\*/g, (_, x) => `< strong>${x}< /strong>`)
  return s
}

// 不包含某字串
/^(?!.*bug)/.test('aaabugaaa')

// string incrementer
function incrementString (string) {
  return string.replace(/[0-9]+$/, char => {
    let i = char.length - 1
    while (char[i]) {
      if (char[i] !== '9') return char.slice(0, i) + (char[i] - -1) + char.slice(i + 1)
      else {
        char = char.slice(0, i) + '0' + char.slice(i + 1)
        i--
      }
    }
    return '1' + char
  })
}
// kangkangbierenzmxd
function incrementString(input) {
  if(isNaN(parseInt(input[input.length - 1]))) return input + '1';
  return input.replace(/(0*)([0-9]+$)/, function(match, p1, p2) {
    var up = parseInt(p2) + 1;
    return up.toString().length > p2.length ? p1.slice(0, -1) + up : p1 + up;
  });
}
// ............
function incrementString(input) {
  return input.replace(/([0-8]?)(9*)$/, function(s, d, ns) {
      return +d + 1 + ns.replace(/9/g, '0');
    });
}

// 匹配字符串中不包含 ef 的单词
/\b(?!\w*ef\w*\b)\w+/

// 1113124 -> 1,113,124
str.replace(/\b(?=(\d{3})+(?!\d))/g, ',')
str.replace(/(?=(...)+$)/g, ',')

// 右边有偶数个"的逗号
,(?=([^"]*"[^"]*"[^"]*)*$)

// 匹配右边有奇数个"的逗号
,(?=[^"]*"[^"]*([^"]*"[^"]*"[^"]*)*$)

// 能被 4 整除
/([13579][26]|[2468][048])$/.test(num)
```

# question

5. 在数组中不使用 splice 删除元素（因为循环 + splice 是 n方）

  - two pointer（但是每次判断这个值需不需要删，用 indexOf 好像也是 O(N) * O(N) ?）

9. Object.entries 以及 forEach 的实现

  - Object.entries()方法返回一个给定对象自身可枚举属性的键值对数组，其排列与使用 for...in 循环遍历该对象时返回的顺序一致（区别在于 for-in 循环也枚举原型链中的属性）

  - 将 Object 转换为 Map

  ```js
  var map = new Map(Object.entries(obj));
  ```

  - 另一种将对象转换成数组的方法：使用 Generator 函数将对象重新包装

  ```js
  function* entries(obj) {
    for (let key of Object.keys(obj)) {
      yield [key, obj[key]];
    }
  }

  for (let [key, value] of entries(obj)) {
    console.log(key, '->', value);
  }
  ```

  - 顺便一提是 for (let i [key, value] of collection) { ... }

16. a good try

  ```js
  (result[value] || (result[value] = [])).push(key)
  ```

26. 深拷贝

  - JSON.stringify
    - 无法实现对函数、RegExp等特殊对象的克隆
    - 会抛弃对象的 constructor ，所有的构造函数会指向 Object
    - 对象有循环引用的时候会报错

    ```js
    /* 需要考虑循环引用的情况 */
    cloneDeep: value => {
      if (value === null || typeof value !== 'object') return value

      var ctor = value.constructor
      var obj

      switch (ctor) {
        case RegExp:
          obj = new ctor(value)
          break
        default:
          obj = new ctor()
      }

      for (var key in value) {
        if (value.hasOwnProperty(key)) {
          obj[key] = sanvvv.cloneDeep(value[key])
        }
      }

      return obj
    },
    ```

27. 冒泡

  - if !swaped return
  - 内层循环递减

30. bind

  ```js
  function bind(f, thisArg, ...fixedArgs) {
    return function(...restArgs) {
      f.apply(thisArg, [...fixedArgs, ...restArgs])
    }
  }

  // bind 用于绑定需要特殊 this 值的函数
  function toArray(val) {
    return [].slice.call(val)
  }

  // 等同于
  var toArray = [].slice.call.bind([].slice)

  // 类似的 trick
  // bind 占用了 Number.call 的第一个参数，所以 map 会把第二个参数 index 传给 bind 之后的 Number.call
  Array.apply(null, Array(3)).map(Number.call.bind(Number))
  ```

31. b instanceof B

  - return `b.__proto__.constructor === B` （可以继续在 `__proto__` 上查找）

33. instanceof、Object.create()

  - new 与 Object.create 的区别
    - `__proto__` 指向的区别，'参数'的区别
    - MDN：Object.create()方法创建一个新对象，使用现有的对象来提供新创建的对象的`__proto__`（（已经存在的对象的新实例））
    - Object.create() 的参数应该是一个对象，如果是构造函数的话表现就会很怪异...（怪异的原因是原型指向了函数而不是对象）
      - 是对象的话就是把这个对象当做新创建的函数的 `.__proto__`，越过了构造函数直接实现继承（差异化继承）

  ```js
  Object.create = function (proto) {
    // ...
    var F = function () {}
    F.prototype = proto // !important
    return new F()
  }

  function INSTANCEOF(val, fn) {
    if (!val) {
      return false
    }
    if (!val.__proto__) {
      return false
    }
    if (val.__proto__.constructor === fn) {
      return true
    } else {
      return INSTANCEOF(val.__proto__, fn)
    }
  }
  ```

33. 排序 series

  - https://www.cnblogs.com/wangfupeng1988/archive/2011/12/26/2302216.html

34. quickSort 快速排序

  ```js
  // simply version with O(n) extra space
  function quickSort (ary) {
    if (ary.length <= 1) return [...ary]

    var pivot = ary[Math.floor(ary.length * Math.random())]
    var left = []
    var middle = []
    var right = []

    for (var i = 0; i < ary.length; i++) {
      if (ary[i] < pivot) left.push(ary[i])
      else if (ary[i] === pivot) middle.push(ary[i])
      else right.push(ary[i])
    }

    return quickSort(left).concat(middle, quickSort(right))
  }

  // better version sort the array in place
  function quickSort2 (ary, start = 0 ,end = ary.length - 1) {
    if (end <= start) return

    var pivotIndex = Math.floor((end - start + 1) * Math.random()) + start
    var pivot = ary[pivotIndex]

    swap(ary, pivotIndex, end)

    for (var i = start - 1, j = start; j <= end; j++) {
      if (ary[j] <= pivot) {
        i++
        swap(ary, i, j)
      }
    }

    quickSort2(ary, start, i - 1)
    quickSort2(ary, i + 1, end)

    return ary
  }

  function swap(arr, a, b) {
    var temp = arr[a]
    arr[a] = arr[b]
    arr[b] = temp
  }
  ```

35. mergeSort 归并排序

  ```js
  function mergeSort (ary) {
    if (ary.length <= 1) return [...ary]

    var mid = ary.length / 2 | 0
    var left = mergeSort(ary.slice(0, mid))
    var right = mergeSort(ary.slice(mid))
    var res = []

    var i = 0, j = 0
    while (i < left.length && j < right.length) {
      if (left[i] <= right[j]) res.push(left[i++])
      else res.push(right[j++])
    }

    return res.concat(left.slice(i), right.slice(j))
  }
  ```

41. IEEE754

  - float：使用1位表示符号，8位表示指数，23位表示尾数（小数部分）
  - double：使用1位表示符号，11位表示指数，52位表示尾数

71. iterator

  - Iterator对象是一个指针对象，实现类似于单项链表的数据结构，通过next()将指针指向下一个节点
  - [Symbol.iterator]（返回的就是 values?）
    - 拥有
      - Array
      - 类数组对象：arguments、NodeList
      - Set、Map
    - 可以生成 Iterator 对象，可以被 for-of 取值
  - for of 可以遍历 iterator（是一个 generator function，当然普通的函数但是实现了 next 的也可以）
  - 扩展运算符也可以遍历 iterator

74. 回溯

```js
var items = []
var sum = ary => ary.reduce((a,b) => a+b)

function targetSum(ary, target, start = 0) {
  for(var i = start; i<ary.length; i++) {
    items.push(ary[i])
    if (sum(items) === target) {
      console.log(items)
    } else if (sum(items) < target) {
      targetSum(ary, target, i + 1)
    }
    items.pop()
  }
}
```

76. callee

```js
var data = [];

for (var i = 0; i < 3; i++) {
    (data[i] = function () {
       console.log(arguments.callee.i) 
    }).i = i;
}
```

77. 事件 

- scroll 的默认事件无法阻止
- 到达目标阶段的事件回调函数（冒泡或捕获）顺序依照注册先后，其他时候都是先捕获后冒泡
- e.stopImmediatePropergation
- 按键事件只能设置在能获取到焦点的dom对象上（可以加一个tabindex="1"），否则只能在docuemnt/window/html上获取到这个按键事件
- 在mouseup事件后，一个click会在同时包含鼠标按下和松手的最内层元素上触发
- clientX、clientY、getBoundingClientRect
- which标识哪个鼠标按钮被按下，0表示无，1表示左键，2表示中键，3表示右键
- buttons表示哪些按钮被按下，用二进制最低位（1）表示左，第二位（2）表示右，第三位（4）表示中，同时按下会求或运算
- 鼠标从父元素进入子元素中会触发父元素的mouseout，但是mouseover又会从子元素冒泡到父元素；鼠标从子元素移出也会触发父元素的mouseout（先触发子元素的out，再冒泡到父元素的out，最后触发父元素的over）
- 现在有mouseenter和mouseleave了，进入子元素不会触发父元素的leave，并且两者都不会冒泡
- mousewheel/DOMMouseScroll：滚轮事件，可以被preventDefault
- focus/blur：不冒泡，focusin/focusout：可以冒泡
  - 想要让不会冒泡的事件能被父元素委托，可以使用捕获（true）

78. dom

- document.querySelectorAll 获取的是 nodeList，是只读的

1.  web workers

处于另一个线程，传递数据的话是完全复制一份新的（而不是指针），跟主线程之间只能通过发送和接收消息来通信，worker 内不能访问 dom 以及其他和 ui 相关接口

worker 中的全局对象是 DedicatedWorderGlobalScope

85. 事件

- domready：DOMContentLoaded、readystatechange（旧），在浏览器解析完 dom 时触发
- window.onload：执行js前可以加载的东西都加载完了时触发（包括图片）
- 拖拽事件
- localStorage：onstorage
- postMessage实现跨域穿消息
- window.onhashchange （#）
- touch 事件是支持多点支持的，可以通过 event.touches 得到所有的点

86. history.pushState

- onpopstate

87. data uri

把资源直接嵌入到 url 里（使用 base64 编码）

`src="data:image/png;charset=utf-8;base64,xxxxxxxxx"`

10kb

不需要建立连接和发送请求，节约网络资源（但是体积会变大，解码也需要时间，也不能复用资源）

88. base64

52 + 10 + '+=' = 64

二进制 -> 按六位分隔 2**6=64 -> 查表转换成 base64

atob()：base64 -> binary

btoa()

```node
b = Buffer.alloc(8)
b.writeDoubleBE(3.14)
b.toString('base64')

b = readFileSync('./test.png')
```

89. 哈希表

90. xml

冗余多、解析困难、XML DOM 不好操作，不容易获取数据

91. https

额外建立一个连接（TLS）来交换公私钥，之后用加密的数据来进行连接（TCP）

92. file api

URL.createObjectUrl(File/Blob)

93. 浅复制

```js
// 1
const obj = { a: 1, b: 2}
const obj2 = { ...obj }
```

94. 模块

```js
function require (filePath) {
  var fileCode = readFile(filePath)
  var fileFunction = new Function('exports', fileCode)
  var exports = {}
  fileFunction(exports)
  return exports
}
```

# node
## process

- process.nextTick
- process.chdir
- process.cwd
- process.stdin
- process.stdout
- process.stderr

## global

- setImmediate
- id.unref

## events

- EventEmitter
- .once

## buffer

- Buffer.alloc
- Buffer.allocUnsafe
- Buffer.from

## http

- http.createServer
- server.on / server.listen
- res.setHeader('Content-Type', 'text/plain; charset=UTF-8;'zf)
- req.url / req.method / req.headers / req.socket

```js

```

## Proxy 数据劫持

```js
function observe(obj) {
  if (!obj || typeof obj !== 'object') return
  var dep = new Dep()
  obj = new Proxy(obj, {
    get: function (target, key, receiver) {
      if (Dep.target) {
        dep.addSub(key, Dep.target)
      }
      return Reflect.get(target, key)
    },
    set (target, key, value, receiver) {
      const val = Reflect.set(target, key, value)
      dep.notify(key)
      return val
    }
  })

  return obj
}

class Dep {
  constructor() {
    this.map = {}
  }

  addSub(key, sub) {
    if (!this.map[key]) this.map[key] = [sub]
    else this.map[key].push(sub)
  }

  notify(key) {
    this.map[key] && this.map[key].forEach(sub => sub.update())
  }
}

Dep.target = null

class Watcher {
  constructor(obj, key, cb) {
    Dep.target = this
    this.obj = obj
    this.key = key
    this.cb = cb
    this.value = obj[key]
    Dep.target = null
    this.update()
  }

  update() {
    this.value = this.obj[this.key]
    this.cb(this.value)
  }
}

var data = {
  message: 1,
  task: 2
}
var ob = observe(data)
function update(value) {
  document.querySelector('#app').innerText = value
}
var wt = new Watcher(ob, 'message', update)
```

