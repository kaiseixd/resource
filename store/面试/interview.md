# JS 
## Ajax

[readme](https://segmentfault.com/a/1190000004322487)

```js
var xhr = new XMLHttpRequest()
xhr.onreadystatechange = function () {
  if (xhr.readyState === 4) {
    if (xhr.status === 200) {
      // code
    } else {
      // err
    }
  }
}
xhr.open('GET', url)
xhr.setRequestHeader('Content-Type', 'application/json')
xhr.send(null)
```

```
setRequestHeader
getResponseHeader
xhr.response / xhr.responseText
xhr.overrideMimeType / xhr.responseType = '' || 'text' || 'document' || 'json' || 'blob' || 'arrayBuffer'
readyState: 0：init，1：opened，2：sended & header received，3：loading response，4：done
xhr.timeout
可 send type：Arraybuffer, Blob, document, DOMString, Formdata, null
xhr.withCredentials：在跨域情况下，想要发送 cookie 需要将该属性设为 true，同时 server 端也要设置 Access-Control-Allow-Credentials: true，否则只在同域情况下能携带 cookie

xhr.onprogress(下载) / xhr.upload.onprogress(上传)
onload：readyState === 4
onabort：xhr.abort()
ontimeout
onerror：只有发生了网络层级别的异常才会触发此事件，对于应用层级别的异常，如响应返回的xhr.statusCode是4xx时，并不属于Network error，所以不会触发onerror事件，而是会触发onload事件
```

## 类数组或可迭代对象转换为数组

```js
Array.from(arrayLikeObject)
[].slice.call(arrayLikeObject)
[].concat.call(arrayLikeObject)
```

## 继承

```js
// 组合寄生继承
function Child() {
  Parent.call(this)
}

var F = new function () {}
F.prototype = Parent.prototype
// 为什么不 Child.protype = new Parent() 呢，因为这样会给 Child.prototype 添加属性
Child.prototype = new F()
Child.prototype.constructor = Child
// Object.setPrototypeof(Child.prototype, Parent,prototype)
// Child.prototype.__proto__ = Parent.prototype

// 2
function Child() {
  Parent.call(this)
}

Child.prototype = Object.create(Parent.prototype, {
  constructor: {
    value: Child,
    enumerabel: false,
    writable: true,
    configurable: true
  }
})

// class
class Child extends Parent {}
```

```
静态方法：static method
静态属性：Foo.prop（static prop = 2 提案中...）
constructor 默认返回 this，也可以提前 return object
```

### es5 继承和 class 继承的区别

1. class内部方法不可枚举（包括原型上的方法和静态方法，当然实例上的是可以的，因为直接通过赋值添加）

2. class不存在变量提升

3. 继承时，子类的 `__proto__` 指向父类，子类原型的 `__proto__` 指向父类原型（使用 Object.setPrototypeof）

4. 构造函数中的 this 区别
  - es5：子类构造函数中的 this 在一开始就是子类的实例 ，之后再调用Parent.call(this) 赋值。
  - class：子类构造函数在一开始就调用super()，在其中创建父类的实例并且给this赋值，之后再在子类构造函数中对其修改。

5. **可以用来实现原生构造函数的继承**

## arguments

- 是所有非箭头函数中都可用的局部变量
- 类数组，除 length 和索引元素之外没有任何 Array 属性，但是可以被转换成一个真正的 Array
- 对 arguments 使用 slice 会阻止引擎对其优化

## 箭头函数

- this：根据声明函数位置的词法作用域来决定this值
- arguments：没有
- prototype：没有
- 不能被 new 调用

## 去抖

```js
export function debounce(func, wait = 100) {
  let timer = 0
  return (...args) => {
    clearTimeout(timer)
    timer = setTimeout(() => {
      func.apply(this, args)
    }, wait)
  }
}

export function throttle(func, wait = 100) {
  let last = 0
  return (...args) => {
    let cur = +new Date()
    if (cur - last > wait) {
      func.apply(this, args)
      last = cur
    }
  }
}

export function debounceMix(func, wait = 100) {
  let timer = 0
  let last = 0
  return (...args) => {
    clearTimeout(timer)
    let cur = +new Date()
    if (cur - last > wait) {
      func.apply(this, args)
      last = cur
    }
    timer = setTimeout(() => {
      func.apply(this, args)
      last = +new Date()
    }, wait)
  }
}
```

## new

- 创建一个实例对象，并让构造函数中的 this 指向该对象（模拟实现的话只能使用 call）
- 将实例对象的 `__proto__` 属性指向原型对象的 `prototype`
- 调用构造函数，如果返回值不是对象的话返回 this，否则返回返回值

```js
function NEW(ctor, ...args) {
  var obj = {}
  var res = ctor.apply(obj, args)
  Object.setPrototypeOf(obj, ctor.prototype)
  if (typeof res === 'object') return res
  else return obj
}
```

## 进制转换

- parseInt('010', 2)
- 114514..toString(2)

## Event Loop

简单来说就是主线程不断循环读取任务队列中的事件的机制。主线程会监听调用栈，当调用栈为空的时候就会从任务队列去出任务加入到调用栈中执行，之后再弹出，不断循环。

任务队列分为 microtask 和 macrotask（又称 task），前者只有一个，后者有多个。

过程：

1. 检查 macrotask 队列，执行最前面的任务(执行js?)
2. 检查 microtask 队列，运行任务直到为空
3. 执行 macrotask 队列
4. 渲染
  1. 执行resize，scroll，媒体查询，动画，全屏等步奏
  2. 运行animation frame回调
  3. 运行IntersectionObserver回调
5. 回到第一步

每一个进程都会有一个 Event Loop。

## 异步循环

使用 for-of、for

自己实现一个 forEach

异步 reduce？

## require 的实现

简单的讲实现就是先声明一个 module = { exports: {} } 和 exports = module.exports，之后获取文件中的代码，并用一层自执行函数包装（字符串拼接），传入 require, module, exports, __dirname, filename，再执行它，这个时候就会修改传入的 module 和 exports，最后将 module.exports 返回。

缓存：用 require.cache 缓存。

```js
if (require.cache[filename]) {
  return require.cache[filename].exports
}

...

require.cache[filename] = module
return module.exports
```

为什么还需要一个 module，只用 exports 不行：因为在代码中给 exports 赋值（想 export 基本值的话）只会指向新的引用，不会修改到传入的 exports。

```js
function require(/* ... */) {
  const module = { exports: {} };
  ((module, exports) => {
    // 模块代码在这。在这个例子中，定义了一个函数。
    function someFunc() {}
    exports = someFunc;
    // 此时，exports 不再是一个 module.exports 的快捷方式，
    // 且这个模块依然导出一个空的默认对象。
    module.exports = someFunc;
    // 此时，该模块导出 someFunc，而不是默认对象。
  })(module, module.exports);
  return module.exports;
}
```

# 浏览器
## 网络解析过程
### DNS 域名解析

输入的 url 由于是域名，TCP/IP 使用 IP 地址来唯一确定一台主机到因特网的连接，需要使用 DNS 来完成域名到 IP 地址的映射工作。

流程：

- 缓存
  - 浏览器缓存 -> 本地host -> 路由器缓存：找到即可
- 服务器查询
  - 本地 DNS 服务器：由 DNS 解析器向本地 DNS 服务器发起 UDP 请求，服务器首先查询是否在本地区域文件或者 DNS 缓存中，再根据是否设置转发器决定是向上一级 DNS 服务器还是根 DNS 服务器发送**代理**请求，最后将应答返回（本机到 DNS 服务器的查询过程称为递归：一路查下去中间不返回，直到得到最终结果）
  - DNS 服务器：根服务器收到请求后，并不解析地址，而是将顶级域 DNS 服务器的 IP 返回给本地 DNS 服务器，让它重新发送请求。同理，顶级域 DNS 服务器在查询完缓存后会将二级域名服务器的地址返回。（DNS 服务器到 DNS 服务器的查询过程称为迭代：查到一个可能知道地址的服务器地址就将此地址返回，重新发送解析请求）

### 建立 TCP 连接

连接步骤：

1. 服务端打开 socket 开始监听连接（被动打开）
  - 进入 LISTEN 状态
1. 客户端向服务端发送一个 SYN 请求报文来主动打开连接
  - 进入 SYN_SEND 状态
  - 序号为随机设定的 A
  - SYN 报文段不带数据，但是会消耗一个序号
2. 服务端收到报文后，如果同意建立连接则返回一个 SYN/ACK 确认报文
  - 进入 SYN_RCVD 状态
  - 序号为随机设定的 B，确认序号为 A+1
3. 客户端收到报文后，返回一个 ACK 确认报文
  - 进入 ESTABLISHED 状态
  - 序号为 A+1，确认序号为 B+1（因为被 SYN 消耗了一个序号所以最终都加了 1）
  - 这个时候一般会携带真正需要传输的数据，如果服务器也收到该数据报文就会同样进入 ESTABLISHED 状态，此时 TCP 连接建立完成

确定超时总共需要：1 + 2 + 4 + 8 + 16 + 32 = 63s

### TLS 握手

HTTPS 在 HTTP 的基础上加了一层 SSL/TLS，增加了身份验证和加密信息。

SSL：Secure Sockets Layer，安全套接层

TLS：Transport Layer Security，传输层安全协议

过程：

验证证书并且确定三个随机值用来生成对称加密需要的密钥。

1. TCP 连接建立后，客户端发送一个随机值，需要的协议和加密方式
2. 服务端收到客户端的随机值，自己也产生一个随机值，并根据客户端需求的协议和加密方式来使用对应的方式，发送自己的证书（如果需要验证客户端证书需要说明）
3. 客户端收到服务端的证书并验证（通过证书关系链从根 CA 验证）是否有效，验证通过会再生成一个随机值，通过服务端证书的公钥去加密这个随机值并发送给服务端，如果服务端需要验证客户端证书的话会附带证书
4. 服务端收到加密过的随机值并使用私钥解密获得第三个随机值，这时候两端都拥有了三个随机值，可以通过这三个随机值按照之前约定的加密方式生成密钥，接下来的通信就可以通过该密钥来加密解密

非对称加密：

需要两个密钥：公开密钥和私有密钥，加密和解密使用的是两个不同的密钥。甲方生成一对密钥之后把私有密钥保存，公有密钥传给乙方，乙方就可以用公有密钥加密信息再传给甲方，由甲方的私有密钥解密。

对称加密：

只使用一个密钥用来加密和解密。

在 TLS 握手阶段，两端使用非对称加密的方式来通信，但是因为**非对称加密损耗的性能比对称加密大**(为什么呢?)，所以在正式传输数据时，两端使用对称加密的方式通信。

### 获得页面响应

重定向响应：如果服务器返回了跳转重定向（非缓存重定向），那么浏览器端就会向新的URL地址重新走一遍DNS解析和建立连接。

200：浏览器开始解析页面，进入准备渲染的阶段。

### 页面资源加载

浏览器在解析页面的过程中会去请求页面中诸如 js、css、img 等外联资源，这些请求也是要建立连接的。

优化：启用长连接，同一客户端 socket 针对同一 socket 后续请求都会复用一个 TCP 连接进行传输；针对 SSL/TLS 握手有会话恢复机制，验证通过后，可以直接使用之前的对话密钥，减少握手往返。

## 工作流程

[render process](https://coolshell.cn/wp-content/uploads/2013/05/Render-Process-768x250.jpg)

主流程：

好像错了？？？跟下面不一样

1. 解析（parse、construct）
  - HTML/SVG/XHTML：构建 DOM tree
  - CSS：构建 CSS Rule Tree
  - JS：通过 api 来操作 DOM Tree 和 CSS Rule Tree 
2. 呈现（render）：通过 DOM Tree 和 CSS Rule Tree 来构建 Rendering Tree
  - 包含多个带有视觉属性的矩形
  - Rendering Tree 并不等同于 DOM Tree
  - Rendering Tree 上的每个节点会被附加 CSS Rule
3. 布局（layout/reflow）：计算每个节点的位置
4. 绘制（paint）：遍历呈现树，并由后端层将每个节点绘制出来

阻塞关系：

- js 在执行的时候会阻塞 DOM 解析（但是还是可以下载其他资源）
- 没有 defer async 的 js 标签在下载的时候也会阻塞 DOM 解析
- CSS 的解析不会阻塞 DOM 的解析，但是会阻塞 DOM 的渲染（因为需要合并成 Rendering Tree 之后渲染）
- 但是 CSS 的解析会阻塞 js 执行（加载不会），而 js 又会阻止其后的 DOM 解析

### DOM 解析

[构建对象模型](https://developers.google.com/web/fundamentals/performance/critical-rendering-path/constructing-the-object-model?hl=zh-cn)

将文档解析成代表文档结构的节点树（解析树或者语法树）

Bytes → characters → tokens → nodes → object model

### CSS 解析

通过与 DOM 解析相同的步骤获得 CSSOM

由于构建 CSSOM 树是一个十分消耗性能的过程，所以应该尽量保证层级扁平，减少过度层叠，越是具体的 CSS 选择器，执行速度越慢。

### render -> layout -> paint

[render tree construction](https://developers.google.com/web/fundamentals/performance/critical-rendering-path/render-tree-construction?hl=zh-cn)

1. 构建 Rendering Tree
  - 遍历 DOM 树的每个可见节点
  - 为其找到适配的 CSSOM 规则并应用它们
  - 发射可见节点，连同其内容和样式
2. 布局
  - 从渲染树的根节点开始遍历
  - 输出多个盒模型（精确地捕获每个元素在视口内的确切位置和尺寸：几何信息）
3. 绘制（栅格化）
  - 根据节点的计算样式和几何信息将其转换成屏幕上的实际像素

### 渲染层合并 composite

对页面中 DOM 元素的绘制是在多个层上进行的。在每个层上完成绘制过程之后，浏览器会将所有层按照合理的顺序合并成一个图层，然后显示在屏幕上

## cookie & session

HTTP 本身是无状态的，需要记录用户状态时就会用到 cookie 和 session。

### cookie

服务器发送到用户浏览器并保存在本地的一小块数据，它会在浏览器下次向同一服务器再发起请求时被携带并发送到服务器上。

一般用于：

1. 会话状态管理（如用户登录状态、购物车、游戏分数或其它需要记录的信息）
2. 个性化设置（如用户自定义设置、主题等）
3. 浏览器行为跟踪（如跟踪分析用户行为等）

缺点：会在用户访问服务器的时候被带上，增大请求包大小。

如何创建：当服务器收到 HTTP 请求时，服务器可以在响应头里面添加一个 Set-Cookie 选项。浏览器收到响应后通常会保存下 Cookie。

保存时间：没有设置 Expires 或者 Max-Age 的话，在会话结束（浏览器关闭）时就会被删除。而设置了则可以给 Cookie 指定一个过期时间（客户端时间） / 有效期。

安全：设置为 Secure 的 Cookie 只应通过被 HTTPS 协议加密过的请求发送给服务端。设置为 HttpOnly 的 Cookie 将无法被 Document.cookie 访问（避免 XSS）。

作用域：Domain 标识指定了哪些主机可以接受 Cookie。如果不指定，默认为当前文档的主机（不包含子域名）。如果指定了 Domain ，则一般包含子域名。Path 标识指定了主机下的哪些路径可以接受 Cookie。

SameSite Cookie

### session

保存在服务端的一种数据结构，用来跟踪用户状态。每个用户都会有一个 session id，对应于存在服务器上的值。

第一次创建 Session 的时候，服务端会在 HTTP 协议中告诉客户端，需要在 Cookie 里面记录一个 session id，以后每次请求把这个会话 ID 发送到服务器，就可以找到对应的 session 了。如果浏览器禁用 cookie 就会把 sid 加到 URL 上。

## 存储

cookie：一般由服务器生成，可设置过期时间，4kb，每次请求都会携带在header中

sessionStorage：页面关闭时就清理，5mb

localStorage：没有过期时间，需要手动清理，5mb

indexedDB：没有过期时间，需要手动清理，大小无限

Service Worker：用来做缓存文件，提高首屏速度

## location

- hash：#右边的内容
  - onhashchange
  - hash 改变时前进后退按钮也会有记录
  - hash 部分不会发往服务器
- host：ip + port
- hostname：ip
- href：total
- origin：protocol + host
- pathname：第一个 / 后面的内容到 ? 为止
- port：port
- protocol：protocol
- search：? 后面的内容到 # 为止
- assign()
- replace()

## XSS

跨站脚本攻击，属于代码注入的一种，攻击者通过拼接js将代码注入网页中，对网页进行篡改，用户访问时就会受代码影响，暴露隐私数据或者发起 CSRF 攻击。利用的是用户对指定网站的信任。

防御方法：

1. 过滤特殊字符 < > script，或者对用户输入进行URL编码
2. 设置cookie时加上HttpOnly参数，能阻止cookie被获取
3. 不要使用innerHTML
4. 通过 Content-Security-Policy Header 开启 CSP，规定了浏览器只能够执行特定来源的代码 Content-Security-Policy: default-src 'self'

### CSP

Content-Security-Policy，HTTP 头，也可以通过页面的 meta 标签来配置，用来限制页面的行为：

- 能（否）使用某个域的资源
- 能（否）连接某个服务器
- 能否被放入iframe，能否在其内放入iframe
- 能否使用内联样式/脚本

CSP 通过指定有效域——即浏览器认可的可执行脚本的有效来源——使服务器管理者有能力减少或消除 XSS 攻击所依赖的载体。一个 CSP 兼容的浏览器将会仅执行从白名单域获取到的脚本文件，忽略所有的其他域的脚本。

```html
<meta http-equiv="Content-Security-Policy" content="default-src 'self'; img-src https://*; child-src 'none';">

服务端配置：Content-Security-Policy: default-src 'self'; img-src *; media-src media1.com media2.com; script-src userscripts.example.com
```

## CSRF

跨站请求伪造，借助用户的cookie，让用户发起请求，完成一些违背用户行为的操作（预先构造好请求参数或者直接伪造用户身份），一般通过XSS，命令行伪造实现，或者让用户在访问黑客编写的网站时提交请求。实际上利用的是网站对用户的信任。

防御方法：

1. 验证 HTTP Referer 字段：但是有些用户设置浏览器不提供 Referer，并且低版本浏览器的 Referer 可以伪造
2. 在请求地址中添加 token 并验证：问题是每个请求都要设置携带 token 太麻烦，而且如果在网站中访问到恶意链接（通过XSS），也会携带上 token
3. 在 HTTP 头中自定义属性并验证：在 XMLHttpRequest 中添加 X-CSRFToken Header，问题是只适用于 XHR 请求
4. 重要的 cookie 存储时间要尽量短，并且重要操作执行前必须要确认

# Vue

[知识点](https://github.com/tangxiangmin/interview/blob/master/%E7%9F%A5%E8%AF%86%E7%82%B9/Vue.md)
[面试题](https://juejin.im/post/59ffb4b66fb9a04512385402)
[面试题](https://www.jianshu.com/p/e54a9a34a773)

## DOM 更新是异步的

只要观察到数据变化，Vue 将开启一个队列（watcher queue），并缓冲在同一事件循环中发生的所有数据改变。如果同一个 watcher 被多次触发，只会被推入到队列中一次。

然后，在下一个的事件循环“tick”中，Vue 刷新队列并执行实际 (已去重的) 工作（watcher 的 run）。Vue 在内部尝试对异步队列使用原生的 Promise.then 和 MessageChannel，如果执行环境不支持，会采用 setTimeout(fn, 0) 代替。

所以想要在数据变化之后马上进行 DOM 操作并能获取到数据，需要将其写在 vm.$nextTick 的回调函数中。

## computed 和 watch 的区别

watch：检测到某一属性的变化后执行相应的回调。

computed：计算属性结果依赖的值变化之后改变计算属性的值。

## 数据绑定

实现双向绑定需要：defineProperty 劫持数据变化，给属性添加发布订阅来通知视图更新。

### observe

遍历 data 属性，给每个属性调用 observeProperty。

observeProperty 创建 Dep 实例，observe 属性的值（遍历子属性），给属性添加 getter 和 setter。

getter 中往 dep 里添加 watcher：创建 watcher 实例的时候将 Dep.target 设为这个 watcher，之后获取依赖的值，触发依赖的 getter，在其中将 Dep.target 添加到其 dep 中。

setter 设置新值并让 dep 通知 watcher 数据变更。

### Dep

Dep.target：watcher 实例。

addsub：将订阅者 watcher 添加到 this.subs 中。

notify：通知 this.subs 中的订阅者数据变更（调用 sub.update()）。

### Watcher

在 constructor 中将 Dep.target 指向自己，之后触发属性的 getter，再清空 Dep.target。

update：调用传入的 cb 进行视图更新。

## 生命周期

- init Events & Lifecycle
- $ beforeCreate
- init Injections & reactivity
- $ created
- 没有 el 选项的话就等待 vm.$mount(el) 调用，然后进入下一步
- 判断是否有 template
  - 没有的话将 el 的 outerHTML 作为 template
  - 有的话将  template 编译成 render 函数
- $ beforeMount
- 创建 vm.$el，并把渲染内容挂载到 DOM 上
- $ mounted
- $ beforeUpdate
- Virtual DOM re-render and patch
- $ updated
- $ beforeDestroy
- teardom watchers, child components and event listeners
- $ destroyed

more：

- activated：keep-alive 组件激活时调用
- deactivated：keep-alive 组件停用时调用
- errorCaptured：当捕获一个来自子孙组件的错误时被调用

### 触发时机

- 初始化组件时仅执行 beforeCreate/Created/beforeMount/mounted 四个钩子函数
- 当改变 data 中的变量时会执行 beforeUpdate/updated 钩子函数

### 父子组件的生命周期

- 初始化时，父组件初始化到 beforeMount 阶段后开始初始化子组件到挂载为止，之后才挂载父组件，并且会主动触发一次更新
- data 改变时各自更新
- props 改变时：p-beforeUpdate -> c-beforeUpdate -> c-updated -> p-updated
- 父组件摧毁时：p-beforeDestroy -> c-beforeDestroy -> c-destroyed -> p-destroyed

## 组件通信
### 父 -> 子

- props
- $children：当前实例的直接子组件，但是并不保证顺序，而且也不是响应式的

### 子 -> 父

- 事件：$emit('func', data)（用第二个参数传参） + @func="handleMsg"
- $parent

### 兄弟

- 使用一个 Vue 实例作为中央事件总线，之后使用 $on $emit 通信

### 多层级父子组件

- $broadcast & $dispatch？（1.0）
  - 作为替代可以手动实现
  - 或者实现一个简易的 vuex 来通信
- provide / inject （2.2+）
  - 适合组件库或插件

### 复杂的单页应用数据管理

vuex

## provide/inject

这对选项需要一起使用，以允许一个祖先组件向其所有子孙后代注入一个依赖，不论组件层次有多深，并在起上下游关系成立的时间里始终生效，类似 React 的 context。

主要为高阶插件/组件库提供用例。

## 自定义组件指令

```js
Vue.directive('my-directive', {
  bind: function (el, binding) {}, // 指定第一次绑定到元素时调用，可进行初始化设置
  inserted: function (el, binding) {}, // 插入父节点时调用
  update: function (el, binding) {}, // 所在组件的 VNode 更新时调用，但是可能发生在其子 VNode 更新之前
  componentUpdated: function (el, binding) {}, // 指令所在组件的 VNode 及其子 VNode 全部更新后调用
  unbind: function (el, binding) {} // 指令与元素解绑时调用
})
```

# React
## setState

每次 setState 都会导致 re-render，即使没有改变任何值，这是因为 shouldComponentUpdate 默认返回 true。

setState 在 React 中是异步的，它会把多次的状态更新整合为一次（相当于 shallow merge），对于同一个状态的多次更新只会执行最后一次。而异步和 DOM 事件是同步的（使用了 React Fiber 后是异步的?）。

想要同步调用，就需要传入函数。因为对象会被 shallow merge，但是函数只会被 queue 起来，下一个函数接收到的 state 都是前一个函数操作后的 state。另一种方法是用第二个参数来执行回调。

```js
this.setState(prev => ({ counter: prev.counter + 1 }))
```

### 异步更新的原理（Batch Update）

Batch Update 对于使用 virtual dom 来更新 view 的 MV* 框架来说很重要，可以合并更新，减少 virtual dom 的更新次数。

是否合并的判断：若 batchingStrategy.isBatchingUpdates 为 true 则将调用了 setState 的组件放入 dirtyComponents 数组中，为 false 则调用 batchedUpdates 方法更新队列中的内容。

batchedUpdates：将 isBatchingUpdates 设为 true，表示正在更新中，之后调用 transaction.perform。

Transaction：它会给函数包装一层 Warpper，在 perform 执行前后额外执行 所有 warpper 的 initialize 和 close 方法。

在 Transaction 的 initialize 阶段，一个 update queue 被创建。在 Transaction 中调用 setState 方法时，状态并不会立即应用，而是被推入到 update queue 中。函数执行结束进入 Transaction 的 close 阶段，update queue 会被 flush，这时新的状态会被应用到组件上并开始后续 Virtual DOM 更新等工作。

比如默认第一次应用初始化的时候是一次事务的进行，在用户交互的时候是一次新的事务开始，会在同一次同步事务中标记 batchUpdate=true，这样的做法是不破坏使用者的代码。

然后如果是 Ajax，setTimeout 等要离开主线程进行异步操作的时候会脱离当前 UI 的事务，这时候再进入此次处理的时候 batchUpdate=false，所以才会 setState 几次就 render 几次。

Vue 的 Batch Update 直接利用了 Event Loop 来实现。

## ref

适合使用 ref 的情况：

- 处理焦点、文本选择或媒体控制。
- 触发强制动画。
- 集成第三方 DOM 库

创建：React.createRef()

当 ref 属性被用于一个普通的 HTML 元素时，React.createRef() 将接收底层 DOM 元素作为它的 current 属性以创建 ref 。

当 ref 属性被用于一个自定义类组件时，ref 对象将接收该组件已挂载的实例作为它的 current 。

函数式组件不能创建因为没有实例，但是内部还是可以使用的，只要它指向一个 DOM 元素或者 class 组件。

```jsx
<input
  type="text"
  ref={(input) => { textInput = input; }} 
/>
```

## 生命周期
### old

挂载：

constructor -> componentWillMount -> render -> componentDidMount

更新：

props change -> componentWillReceiveProps -> shouldComponentUpdate -> componentWillUpdate -> render -> componentDidUpdate

state change -> shouldComponentUpdate -> componentWillUpdate -> render -> componentDidUpdate

卸载：

componentWillUnmount

### new 16.3

挂载：

constructor -> getDerivedStateFromProps -> render -> componentDidMount

更新：

props or state change -> getDerivedStateFromProps -> shouldComponentUpdate -> render -> getSnapshotBeforeUpdate -> componentDidUpdate

卸载：

componentWillUnmount

### 可打断的生命周期

componentWillMount, 
componentWillReceiveProps, 
shouldComponentUpdate, 
componentWillUpdate, 
是可以被打断的，这样可能会多次调用这些生命周期函数（开启 async rendering 的时候），所以在 v16 中添加了新的 API，并预备移除则三个不安全的旧生命周期（除了 shouldComponentUpdate）。

使用 static getDerivedStateFromProps 替代 componentWillReceiveProps，该函数会在初始化和 update 时调用（props 或 state 更新），另外需要注意的是不能使用 this。

getSnapshotBeforeUpdate 在 render 之后，DOM 渲染之前调用，componentWillUpdate 在 render 之前调用。

### 用法建议

```js
class ExampleComponent extends React.Component {
  // 用于初始化 state
  constructor() {}
  // 用于替换 `componentWillReceiveProps` ，该函数会在初始化和 `update` 时被调用
  // 因为该函数是静态函数，所以取不到 `this`
  // 如果需要对比 `prevProps` 需要单独在 `state` 中维护
  static getDerivedStateFromProps(nextProps, prevState) {}
  // 判断是否需要更新组件，多用于组件性能优化
  shouldComponentUpdate(nextProps, nextState) {}
  // 组件挂载后调用
  // 可以在该函数中进行请求或者订阅（添加事件）
  componentDidMount() {}
  // 用于获得最新的 DOM 数据
  getSnapshotBeforeUpdate() {}
  // 组件即将销毁
  // 可以在此处移除订阅，定时器等等
  componentWillUnmount() {}
  // 组件销毁后调用
  componentDidUnMount() {}
  // 组件更新后调用
  componentDidUpdate() {}
  // 渲染组件函数
  render() {}
  // 以下函数不建议使用
  UNSAFE_componentWillMount() {}
  UNSAFE_componentWillUpdate(nextProps, nextState) {}
  UNSAFE_componentWillReceiveProps(nextProps) {}
}
```

# todo
## WebSocket

解决了 HTTP 服务度不能主动推送到客户端的问题。

- 支持双向通信
- 较少的控制开销。连接创建后，ws客户端、服务端进行数据交换时，协议控制的数据包头部较小。在不包含头部的情况下，服务端到客户端的包头只有2~10字节（取决于数据包长度），客户端到服务端的的话，需要加上额外的4字节的掩码。而HTTP协议每次通信都需要携带完整的头部

## 调用栈

用来存储方法调用和基础数据类型的地方。

由于函数需要在执行结束后跳转回调用该函数的代码位置，因此计算机必须记住函数调用的上下文，调用栈就是存储这个上下文的区域。每当函数调用时，当前的上下文信息会被存储在栈顶，函数返回时系统会删除在栈顶的上下文信息，并使用该信息继续执行程序。递归过多会导致栈存储空间过大（栈空间溢出）。

## 不带 referer

```html
<meta name="referrer" content="no-referer">
<img src="xxxx.jpg"  referrerPolicy="no-referrer">
```

## 请求头和响应头

```
GET /a/b.html?a=b&c=d HTTP/1.1
Host: www.example.com
User-Agent: ghjghjk

HTTP/1.1 200 OK
Date:
Content-Length:
Content-Type: 
```

## SQL 注入

## 垃圾回收

(click me)[https://yuchengkai.cn/docs/zh/frontend/#v8-%E4%B8%8B%E7%9A%84%E5%9E%83%E5%9C%BE%E5%9B%9E%E6%94%B6%E6%9C%BA%E5%88%B6]
(2)[https://segmentfault.com/a/1190000000440270]

新生代：

新分配的对象放入 From 空间，满的时候启动算法检查存活对象，将其复制（BFS）到 To 空间，非活跃对象则进行释放，完成复制后将 From 和 To 空间对换。

进入老生代空间的标准：

1. 复制到 To 空间时发现已经经历过一个新生代的清理。
2. 复制到 To 空间时发现 To 空间已经被使用了超过 25%。

老生代：

占用内存较多（1.4GB），使用标记清除算法和标记整理算法。

标记清除（Mark-Sweep）：

遍历堆中的所有对象，并标记那些活着的对象，然后进入清除阶段，只清除没有被标记的对象。

标记清除有一个问题就是进行一次标记清楚后，内存空间往往是不连续的，会出现很多的内存碎片。如果后续需要分配一个需要内存空间较多的对象时，如果所有的内存碎片都不够用，将会使得V8无法完成这次分配，提前触发垃圾回收。

当碎片超过一定限制后会启动标记整理（压缩）算法。

标记整理（Mark-Compact）：

标记整理在标记清除的基础进行修改，将其的清除阶段变为紧缩极端。在整理的过程中，将活着的对象向内存区的一段移动，移动完成后直接清理掉边界外的内存。

紧缩过程涉及对象的移动，所以效率并不是太好，但是能保证不会生成内存碎片。

## Promise
### Promise.all

`Promise.all(iterable)`

返回一个 promise 实例，在参数 iterable 内的所有 promise 都完成或不包含 promise 时 resolved，如果参数中某一个 promise 失败了，则 rejected，失败原因是第一个失败的 promise 的结果。

```js
const [a, b] = await Promise.all([get(url), get(url2)]) // 同时获取
``` 

### Promise.race

`Promise.race(iterable)`

返回一个 promise 实例，由 iterable 中的第一个完成或失败的 promise 决定结果。

## Promise 实现
### constructor

尝试运行传入的参数，excutor(resolve, reject)。

### then

获取 promise 的 self.data，新建一个 promise2，执行 resolvePromise(promise2, value, resolve, reject)。

如果是 pending 就将上面这个函数加入 callbacks，等 resolved 或 rejected 时调用。

### resolvePromise

按照规范 2.3 来。



### Promise.all

创建一个数组，在 for 循环中 Promise.resolve 参数（数组）里的所有项，并用 .then 获取结果，赋值到新建数组[index] 上，在处理到 for 循环最后一个 item 后返回 resolve(resultArray) 

## DataURL

前缀为 data: 协议的的 URL，其允许内容创建者向文档中嵌入小文件。

`data:text/plain;base64,SGVsbG8sIFdvcmxkIQ%3D%3D`

`canvas.toDataURL([type, encoderOptions]);`

优势：

- 不用发送 HTTP 请求，适合小资源

劣势：

- 客户端需要解码。且它解码过程在移动端消耗稍多
- 编码后的尺寸是之前的130%
- 无法缓存

ObjectURL：

一种用于展示 File object 或 Blob object 的 URL。

```js
var objectURL = URL.createObjectURL(object);
var img = document.createElement('img')
img.src = objectURL
```

## virtual dom (vue)

将真实 DOM 树抽象成一个由 js 对象构成的抽象树，进行 DOM 操作的时候只操作 js 对象，之后再通过 diff 算法得出需要修改的最小单位，对这些小单位对应的视图进行更新，这样就可以减少很多 DOM 操作。

由于 DOM 是多叉树结构，完成地比较差异的话需要的时间复杂度会是 O(n^3)，所以 React 团队做出假设，只在同层比较，这样就实现了 O(n) 复杂度的 diff 算法。

### update

数据被修改时，通过 setter 让 Dep notify 所有的订阅者（Watcher），执行 vm._update(vm._render(), hydrating)。

update：

vm._render 就是新的 vnode，之后调用 `vm.__patch__(preVnode, vnode)` 进行 patch。

### patch

比较新老 vnode 节点，根据最小变化单位修改视图。

首先判断新旧节点是否存在，旧节点不存在则用新节点代替它，新节点不存在则删除旧节点。

然后判断两个节点时候是相同类型（sameVnode），是则调用 patchVnode，否则直接用新节点替换旧节点。

### patchVnode

（仿写的时候将 text property children 分开了，所以独立于 children 写了替换文本和属性，和实际 vue 的实现不一样）

如果新旧VNode都是静态的，同时它们的key相同（代表同一节点），并且新的VNode是clone或者是标记了once（标记v-once属性，只渲染一次），那么只需要替换elm以及componentInstance即可。

新旧节点均有子节点则调用 updateChildren，即 diff 操作。

新节点没有子节点则删除旧节点的所有子节点。

旧节点没有子节点则加入新节点的子节点。

当新老节点都无子节点的时候，只是文本的替换。

### updateChildren (diff)

newStartVNode, newEndVNode 和 oldStartVNode, oldEndVNode 之间两两比较。

两者 start 或 end 是 sameVNode 的情况，直接进行 patch 即可，之后将 index 往中间移动。

两个 start 与 end 是 sameVNode 的情况，首先 patch，然后将 oldStartVNode 移到 oldEndVNode 后面，或者将 oldEndVNode 移到 oldStartVNode 前面，index 照样往中间移动。

新旧节点首尾没有 sameVNode 的情况，先生成一个旧节点的 key-to-index map，判断 ch[map[key]] 是否是 sameVNode，是则进行 patch，并将这个旧节点添加到 oldStartVNode 前面，否则用 newStartVNode 创建一个节点，然后 newStartIdx 往后移一位。

在新或旧节点遍历完（startIdx === endIdx）时结束循环，之后根据情况删除尾部多余旧节点或添加新节点。

## 模板引擎实现原理

用特定的dls表达出一段内容中的特定部分需要以各种形式被插入不同的数据，
模板引擎往往会先模板字符串编译为模板函数（为的是后续不需要每次都解析模板字符串），
之后再调用模板函数并传入数据，得到模板被数据插值之后的内容。

## http 2.0 的特性

https://yuchengkai.cn/docs/zh/cs/#http-2-0

## 简述你对tcp协议udp协议及http协议的理解，并解释tcp与http的区别

```
tcp：
  基于连接。保证数据按顺送达。
  服务器监听在某个端口，客户主动发起连接。
  连接建立完成，即成为一个字节流的双向管道。

udp：
  基于消息，无连接，不保证送达。
  任何时候任意两个udp端口之间都可以相互通信。
  通信以单个消息为单位。

http:
  基于tcp的请求响应模型：
    建立tcp连接，客户端发送请求，服务器返回响应。tcp连接断开。

tcp与http的区别：
  http整个报文（包括请求头和请求体）都是tcp的payload（载荷）
  http的payload则是http自身的请求/响应体。
  http是只能在tcp上发送http格式的报文。
```

## tracert

此命令可以得出ip数据包到达目的地址所经过的中间路由。

其原理是借助ip协议的ttl字段及ip数据包的ttl被减为0以后，那个路由会告诉源地址，因为ttl减为0导致包不可达。反馈的协议是ICMP。

实际应用中，一般会发送tcp握手包。

### 事件

- stream.on('data')：数据流收到数据
- stream.on('readable)：数据流向外提供数据
- stream.on('drain')：恢复 writer

## Process

- process.cwd()：获取当前工作目录
- process.env：获取环境变量配置
- process.nextTick：在每个 Event Loop 阶段结束后执行 nextTickQueue 中的 'Tick'

### 标准流

由于 process.stdin 和 process.stdout 都是流形式的，所以必须通过 pipe 作为中介，它们也都部署了 Stream 接口，可以使用其方法（on 啦子类的）。

## Child Process

## Koa2

app.use：

将传入的函数存入 this.middleware 数组，如果是 generator 函数还需要首先通过 koa-convert 转换成类似 async/awaith 函数（generator + 自执行）。

app.listen：

`http.createServer(this.callback()).listen(port)`

每次请求都会执行 callback 回调。

app.callback：

组合 fn = koa-compose(middleware)，创建 ctx = createContext(req, res)，返回处理请求的结果 handleRequest(ctx, fn)（洋葱式调用）。

middleware：

通过 app.use 被传入 middleware 的中间件是一个 async 函数，接收参数 ctx 和 next，ctx 封装了 req 和 res，next 用于将程序控制权交给下一个中间件。

koa-compose：

返回一个以 context 和 next 作为参数的函数，在其中递归地执行所有中间件函数，每个函数在遇到 await next 的时候就开始执行下一个函数，所以中间件函数的执行顺序就是由调用栈来决定的，先执行的函数在遇到 await next 之后剩下的部分反而会后执行。等执行到 middleware 数组中最后一个函数的时候...

```js
// usage: 
app.use(async (ctx, next) => {
  console.log(1)
  // 这里的 next 就是 () => dispatch(i + 1)
  // 会递归地去执行下一个中间件函数
  await next
  console.log(2)
})

// test
// 如果不是 async / generator
// 如果不调用 await next
// 如果调用的是 next

function compose (middleware) {
  // 执行所有中间件的方法
  return function (context, next) {
    let index = -1
    return dispatch(0)
    function dispatch (i) {
      index = i
      let fn = middleware[i]
      if (i === middleware.length) fn = next
      if (!fn) return Promise.resolve()
      try {
        // fn 是 async 函数，这里相当于 Promise.resolve 了一个 promise
        return Promise.resolve(fn(context, function next () {
          return dispatch(i + 1)
        }))
      } catch (err) {
        return Promise.reject(err)
      }
    }
  }
}
```

createContext：

把 res、req 及一些 request 的关键属性挂载到 context 上。

```js
createContext(req, res) {
  const context = Object.create(this.context)
  const request = context.request = Object.create(this.request)
  const response = context.response = Object.create(this.response)
  context.app = request.app = response.app = this
  context.req = request.req = response.req = req
  context.res = request.res = response.res = res
  request.ctx = response.ctx = context
  request.response = response
  response.request = request
  context.originalUrl = request.originalUrl = req.url;
  context.cookies = new Cookies(req, res, {
    keys: this.keys,
    secure: request.secure
  })
  request.ip = request.ips[0] || req.socket.remoteAddress || ''
  context.accept = request.accept = accepts(req)
  context.state = {}
  return context
}
```

handleRequest：

```js
handleRequest(ctx, fnMiddleware) {
  const res = ctx.res
  res.statusCode = 404
  const onerror = err => ctx.onerror(err)
  const handleResponse = () => respond(ctx)
  onFinished(res, onerror)
  return fnMiddleware(ctx).then(handleResponse).catch(onerror)
}
```

koa-convert：

将 generator 和用自执行函数包装。

```js
function co(gen) {
  var ctx = this;
  var args = slice.call(arguments, 1);

  // 统一返回一个整体的 promise
  return new Promise(function(resolve, reject) {
    // 如果是函数，调用并取得 generator 对象
    if (typeof gen === 'function') gen = gen.apply(ctx, args);
    // 如果根本不是 generator 对象（没有 next 方法），直接 resolve 掉并返回
    if (!gen || typeof gen.next !== 'function') return resolve(gen);

    // 入口函数
    onFulfilled();

    function onFulfilled(res) {
      var ret;
      try {
        // 拿到 yield 的返回值
        ret = gen.next(res);
      } catch (e) {
        // 如果执行发生错误，直接将 promise reject 掉
        return reject(e);
      }
      // 延续调用链
      next(ret);
    }
    function onRejected(err) {
      var ret;
      try {
        // 如果 promise 被 reject 了就直接抛出错误
        ret = gen.throw(err);
      } catch (e) {
        // 如果执行发生错误，直接将 promise reject 掉
        return reject(e);
      }
      // 延续调用链
      next(ret);
    }
    function next(ret) {
      // generator 函数执行完毕，resolve 掉 promise
      if (ret.done) return resolve(ret.value);
      // 将 value 统一转换为 promise
      var value = toPromise.call(ctx, ret.value);
      // 将 promise 添加 onFulfilled、onRejected，这样当新的promise 状态变成成功或失败，就会调用对应的回调。整个 next 链路就执行下去了
      if (value && isPromise(value)) return value.then(onFulfilled, onRejected);
      // 没法转换为 promise，直接 reject 掉 promise
      return onRejected(new TypeError('You may only yield a function, promise, generator, array, or object, '
        + 'but the following object was passed: "' + String(ret.value) + '"'));
    }
  });
}
```

错误处理：

通过 handleRequest 最后的 `fnMiddleware(ctx).then(handleResponse).catch(onerror)` catch 到执行中间件过程中的错误。

```js
// usage
app.on('error', err => {
  log.error('server error', err)
})

onerror(err) {
  this.app.emit('error', err, this)
}
```
