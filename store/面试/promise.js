var PENDING = 0;
var FULFILLED = 1;
var REJECTED = 2;

function isFunction(func) {
  return typeof func === 'function';
}
function isObject(obj) {
  return typeof obj === 'object';
}
function isArray(array) {
  return Object.prototype.toString.call(array) === '[object Array]';
}

function Promsie(resolver) {
  if (!isFunction(resolver)) {
    throw new TypeError('resolver must be a function');
  }
  this.state = PENDING;
  this.value = undefined;
  this.queue = [];
  safelyResolveThen(this, resolver);
}

function safelyResolveThen(self, then) {
  var called = false;
  try {
    then(function(value) {
      if (called) {
        return;
      }
      called = true;
      doResolve(self, value);
    }, function(error) {
      if (called) {
        return;
      }
      called = true;
      doReject(self, error);
    });
  } catch(error) {
    if (called) {
      return;
    }
    called = true;
    doReject(self, error);
  };
}

function doResolve(self, value) {
  try {
    var then = getThen(value);
    if (then) {
      safelyResolveThen(self, then);
    }
  }
}

function doReject(self, error) {

}

Promise.prototype.then = function() {

}

Promise.prototype.catch = function() {

}

var promise = new Promise((resolve) => {
  setTimeout(() => {
    resolve('haha')
  }, 1000)
});

promise.then(data => console.log(data));

// Promise.resolve = function() {

// }

// Promise.reject = function() {

// }

// Promise.all = function() {

// }

// Promise.race = function() {

// }
