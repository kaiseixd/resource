### docker能做什么
隔离应用依赖
创建应用镜像并复制
创建容易分发的即启即用应用
允许实例简单、快速地扩展
测试应用并随后销毁它们

Docker 背后的想法是创建软件程序可移植的轻量容器，让其可以在任意安装了 Docker 的机器上运行，而不用关系底层操作系统。

### 概念
#### 镜像
类似虚拟机的快照。

#### 容器
可以从镜像中创建容器实例，类似从快照中创建虚拟机。
一个容器一个进程。
停止一个 docker 容器时，对初始状态（镜像）做的所有变化将会消失。

#### 数据卷
容器数据持久化。
Docker 允许定义应用部分和数据部分，数据卷保存在运行 Docker 的宿主文件系统上（容器之外），而容器则是短暂和一次性的。

#### 链接
容器启动时，将被分配一个随机的私有 IP，其他容器可以使用这个 IP 地址与其进行通讯。

### 为什么能做这些？
#### Cgroups
提供容器隔离。

限制 Linux 进程组的资源占用
为进程组制作 PID、UTS、IPC、网络、用户及装载命名空间

#### union文件系统
用于保存镜像并使容器变得短暂。

文件系统可以被装载在其他文件系统上，每个装载的文件系统表示前一个文件系统之后的变化集合。

### 参考
[Docker Docs](https://docs.docker.com/get-started/)
[Docker入门实战](https://yuedu.baidu.com/ebook/d817967416fc700abb68fca1)
[Docker从入门到实践](https://vuepress.mirror.docker-practice.com/)

### 想要看
https://github.com/bingohuang/play-docker-images