## 流程

初始化：

从配置文件和 Shell 语句中读取与合并配置参数，使用参数实例化 Compiler，加载 Plugin。

编译：

执行对象的 run 方法开始编译，从入口文件出发，针对每个 Module 串行调用对应的 Loader 去翻译文件内容，再找到该 Module 依赖的 Module，递归地进行编译处理。

输出：

对编译后的 Module 组合成一个个包含多个模块的 Chunk，把 Chunk 转换成一个单独的文件加入到输出列表，在配置好确定的输出路径和文件名后输出到文件系统。

Compiler：

负责文件监听和启动编译，包含了完整的 Webpack 配置，全局只有一个 Compiler 实例。

[refer](https://juejin.im/entry/5b0e3eba5188251534379615)

## input

## output

## loader

用于对模块的源代码进行转换（如 TS -> JS），或在 import 模块（如 CSS）时预处理文件。

loader 是链式传递的，从后往前对上一个 loader 处理后的资源进行转换，最后返回预期的 JS。

### 配置
#### css

css-loader：负责把 css 交给下一个 loader（加载成 css 模块?）。

style-loader：把 css 添加到标签里。

less-loader：将 less 转义成 css。

postcss-loader：将 css 解析成 AST 再应用插件。

## plugins

是一个具有 apply 方法的 JavaScript 对象，apply 属性会被 webpack compiler 调用，并且 compiler 对象可在整个编译生命周期访问。

用于解决 loader 无法实现的其他事。

### compiler

### compilation 

代表了一次单一的版本构建和生成资源。

当运行 webpack 时，每当检测到一个文件变化，一次新的编译将被创建，从而生成一组新的编译资源。一个编译对象表现了当前的模块资源、编译生成资源、变化的文件、以及被跟踪依赖的状态信息。

### 事件钩子

run、compile、compilation、emit 等。

### 配置

- html-webpack-plugin：自动生成一个 HTML5 文件（build 用），并且自动引用 bundle 和相关 assets 文件
- mini-css-extract-plugin：vue-loader 中用于提取 js 文件中的 css？
- case-sensitive-paths-webpack-plugin：防止不同的系统下对于大小写的问题导致路径出错
- InterpolateHtmlPlugin：配合 html-webpack-plugin 一起使用，允许你在index.html中使用变量
- WatchMissingNodeModulesPlugin：安装完缺失的 module 后自动重启 webpack
- https://juejin.im/entry/5b0d5fa5518825153d28aec4

## 配置
### SourceMap

信息文件，存储着打包后代码的位置信息，这样就可以在报错信息中定位到代码的位置。

#### 配置

https://segmentfault.com/a/1190000008315937
https://juejin.im/post/58293502a0bb9f005767ba2f
https://segmentfault.com/a/1190000004280859

```
eval： 生成代码 每个模块都被eval执行，并且存在@sourceURL

cheap-eval-source-map： 转换代码（行内） 每个模块被eval执行，并且sourcemap作为eval的一个dataurl

cheap-module-eval-source-map： 原始代码（只有行内） 同样道理，但是更高的质量和更低的性能

eval-source-map： 原始代码 同样道理，但是最高的质量和最低的性能

cheap-source-map： 转换代码（行内） 生成的sourcemap没有列映射，从loaders生成的sourcemap没有被使用

cheap-module-source-map： 原始代码（只有行内） 与上面一样除了每行特点的从loader中进行映射

source-map： 原始代码 最好的sourcemap质量有完整的结果，但是会很慢
```

### PostCSS

负责把 CSS 解析成语法树（AST），这样就能通过插件使用 JS 来处理 CSS，比如添加变量和 mixin 的支持，添加浏览器相关前缀。

#### 插件

Autoprefixer：用于添加浏览器特定前缀。

cssnext：将 CSS 新特性转义成当前浏览器可以使用的语法。

[refer](https://www.ibm.com/developerworks/cn/web/1604-postcss-css/index.html)